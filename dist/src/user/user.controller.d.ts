import { UserService } from "./user.service";
import { User } from "./user.entity";
export declare class UserController {
    private readonly userService;
    constructor(userService: UserService);
    all(): Promise<User[]>;
    add(user: User): Promise<User>;
}
