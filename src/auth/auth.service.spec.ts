import { JwtService } from '@nestjs/jwt';
import { Test, TestingModule } from '@nestjs/testing';
import { AuthService } from './auth.service';
import { UserService } from '../core/services/user.service';
import { StatusService } from '../core/services/status.service';

describe('AuthService', () => {
    let service: AuthService;

    const usersService = {
        findUserByUsername() { },
        validateUserPassword() { },
        addUser() { },
        validate() { }
    };

    const jwtService = {
        signAsync() { },
    };

    const statusService = {
        saveStatus() { },
    };

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            providers: [
                AuthService,
                {
                    provide: UserService,
                    useValue: usersService,
                },
                {
                    provide: JwtService,
                    useValue: jwtService,
                },
                {
                    provide: StatusService,
                    useValue: statusService,
                },
            ],
        }).compile();

        service = module.get<AuthService>(AuthService);
    });

    describe('', () => {
        it('Should call login once with correct user.', async () => {
            // arrange
            const testUserDTO = {
                name: 'user',
                password: 'P@ssw0rd'
            };
            const username = { name: 'user' };
            const fakeToken = 'token';
            const jwtServiceSignSpy = jest
                .spyOn(jwtService, 'signAsync')
                .mockImplementation(() => Promise.resolve(fakeToken));
            // act
            await service.login(testUserDTO);
            // assert
            expect(jwtServiceSignSpy).toBeCalledTimes(1);
            expect(jwtServiceSignSpy).toBeCalledWith(username);
            jwtServiceSignSpy.mockRestore();
        });

        it('Should return correct token.', async () => {
            // arrange
            const testUserDTO = {
                name: 'user',
                password: 'P@ssw0rd'
            };
            const fakeToken = 'token';
            const jwtServiceSignSpy = jest
                .spyOn(jwtService, 'signAsync')
                .mockImplementation(() => Promise.resolve(fakeToken));
            // act
            const result = await service.login(testUserDTO);
            // assert
            expect(result).toEqual('token');
            jwtServiceSignSpy.mockRestore();
        });

        it('Should return correct password.', async () => {
            // arrange
            const testUserDTO = {
                name: 'user',
                password: 'P@ssw0rd'
            };
            const userServiceValidateUserPasswordSpy = jest
                .spyOn(usersService, 'validateUserPassword')
                .mockImplementation(() => Promise.resolve(testUserDTO.password));
            // act
            const result = await service.validateUserPassword(testUserDTO);
            // assert
            expect(result).toEqual('P@ssw0rd');
            userServiceValidateUserPasswordSpy.mockRestore();
        });

        it('Should return existing user.', async () => {
            // arrange
            const testUserDTO = {
                name: 'user',
                password: 'P@ssw0rd'
            };
            const userServicefindUserByUsernameSpy = jest
                .spyOn(usersService, 'findUserByUsername')
                .mockImplementation(() => Promise.resolve(testUserDTO.name));
            // act
            const result = await service.validateIfUserExists(testUserDTO.name);
            // assert
            expect(result).toEqual('user');
            userServicefindUserByUsernameSpy.mockRestore();
        });

        it('Should validate payload.', async () => {
            // arrange
            const testUserDTO = {
                name: 'user',
                password: 'P@ssw0rd'
            };
            const userServiceValidateSpy = jest
                .spyOn(usersService, 'validate')
                .mockImplementation(() => Promise.resolve(testUserDTO));
            // act
            const result = await service.validateUser(testUserDTO);
            // assert
            expect(result).toEqual(testUserDTO);
            userServiceValidateSpy.mockRestore();
        });

        it('Should register new user.', async () => {
            // arrange
            const testUserDTO = {
                name: 'user',
                email: 'user@user.com',
                password: 'P@ssw0rd'
            };
            const userServiceRegisterSpy = jest
                .spyOn(usersService, 'addUser')
                .mockImplementation(() => Promise.resolve(testUserDTO));
            // act
            const result = await service.register(testUserDTO);
            // assert
            expect(result).toEqual(testUserDTO);
            userServiceRegisterSpy.mockRestore();
        });

    });
});

