import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { UserService } from '../core/services/user.service';
import { JwtPayload } from '../core/interfaces/jwt-payload';
import { User } from '../entities/user.entity';
import { UserRegisterDTO } from '../models/user-register-dto';
import { UserLoginDTO } from '../models/user-login-dto';
import { Status } from '../entities/status.entity';
import { StatusService } from '../core/services/status.service';
import { UserReturnDTO } from '../models/user-return-dto';

@Injectable()
export class AuthService {
    constructor(
        private readonly jwtService: JwtService,
        private readonly usersService: UserService,
        private readonly statusService: StatusService,
    ) {}

    async validateIfUserExists(username: string): Promise<User> | undefined {
        return await this.usersService.findUserByUsername(username);
    }

    async validateUserPassword(user: UserLoginDTO): Promise<boolean> {
        return await this.usersService.validateUserPassword(user);
    }

    async register(user: UserRegisterDTO): Promise<UserReturnDTO> {
        const userStatus = new Status();
        await this.statusService.saveStatus(userStatus);
        return await this.usersService.addUser(user, userStatus);
    }

    async login(user: UserLoginDTO): Promise<string> {
        const payload = { name: user.name };
        return await this.jwtService.signAsync(payload); // returns token
    }

    async validateUser(payload: JwtPayload): Promise<User> | undefined {
        return await this.usersService.validate(payload);
    }
}
