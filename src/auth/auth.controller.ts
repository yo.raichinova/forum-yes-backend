import { Controller, Post, Body, ValidationPipe, BadRequestException, Delete, UseGuards, HttpCode, HttpStatus, UseFilters } from '@nestjs/common';
import { AuthService } from './auth.service';
import { UserRegisterDTO } from '../models/user-register-dto';
import { UserLoginDTO } from '../models/user-login-dto';
import { AuthGuard } from '@nestjs/passport';
import { BadRequestFilter } from '../common/filters/bad-request.filter';
import { TransformService } from '../core/services/transform.service';
import { UserReturnDTO } from 'src/models/user-return-dto';

@Controller('')
export class AuthController {
    constructor(
        private readonly authService: AuthService,
        private readonly transformService: TransformService) {}

    @Post('registration')
    @HttpCode(HttpStatus.CREATED)
    async registerUser(
        @Body(new ValidationPipe({ whitelist: true, transform: true }))
        user: UserRegisterDTO) {
        const result = await this.authService.validateIfUserExists(user.name);
        if (result) {
            throw new BadRequestException('User with such username already exists!');
        }
        return await this.authService.register(user);
    }

    @Post('/session')
    @HttpCode(HttpStatus.OK)
    @UseFilters(BadRequestFilter)
    async loginUser(
        @Body(new ValidationPipe({ whitelist: true, transform: true }))
        user: UserLoginDTO,
    ): Promise<{ user: UserReturnDTO, token: string }>  {
        const userFound = await this.authService.validateIfUserExists(user.name);

        if (!userFound) {
            throw new BadRequestException('User with such username does not exist!');
        }
        await this.authService.validateUserPassword(user);
        const token = await this.authService.login(user);
        const userDisplayed = await this.transformService.transformUser(userFound);
        return { user: userDisplayed, token };
        // return await this.authService.login(user); //returns token
    }

    @Delete('/session')
    @HttpCode(HttpStatus.OK)
    @UseGuards(AuthGuard('jwt'))
    public async logoutUser() {
        return { message: 'Successful logout!' };
    }
}