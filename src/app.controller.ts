import { Controller, Get, UseGuards, Req } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';

@Controller()
export class AppController {

  @Get()
  @UseGuards(AuthGuard('jwt'))
  root(@Req() request: any): { data: string } {

    return {
      data: `Only logged users can see this data.`,
    };
  }
}
