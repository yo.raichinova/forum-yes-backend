import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, UpdateResult, Like } from 'typeorm';
import { Posst } from '../entities/post.entity';
import { User, UserRole } from '../entities/user.entity';
import { PostDisplayDTO } from '../models/post-display-dto';
import { PostAddAndUpdateDTO } from '../models/post-add-and-update-dto';
import { PostNotFound, BlockedAccessException } from '../common/filters/post-system-exception';
import { PostQueryDTO } from '../models/post-query-dto';
import { TransformService } from '../core/services/transform.service';

@Injectable()
export class PostService {
  constructor(
    @InjectRepository(Posst)
    private readonly postRepository: Repository<Posst>,
    private readonly transformService: TransformService,
  ) { }

  async findAllByQuery(query: PostQueryDTO): Promise<PostDisplayDTO[]> {
    if (query.text) {
      const searchTerm = query.text;

      const result = await this.postRepository.find({

        where: [
          { title: Like(`%${searchTerm}%`), isDeleted: false },
          { content: Like(`%${searchTerm}%`), isDeleted: false },
        ],
        order: {
          updatedAt: 'DESC',
        },
        relations: ['user', 'likedBy', 'dislikedBy'],
      });

      if (result.length === 0) {
        throw new PostNotFound('No posts with this search phrase were found');
      }
      return await this.transformService.transformPostArray(result);
    }
    const postArr = await this.postRepository.find({
      relations: ['user', 'likedBy', 'dislikedBy'], where: { isDeleted: false }, order: {
        updatedAt: 'DESC',
      } });
    return await this.transformService.transformPostArray(postArr);
  }

  async findPostById(id: string): Promise<Posst> {

    const postFound = await this.postRepository.findOne({ relations: ['user', 'likedBy', 'dislikedBy'], where: { id: +id, isDeleted: false } });
    if (!postFound) {
      throw new PostNotFound('Post with such id does not exist');
    }
    return postFound;
  }

  async transformPostForDisplay(post: Posst): Promise<PostDisplayDTO> {
    return await this.transformService.transformPost(post);
  }

  async add(post: PostAddAndUpdateDTO, user: User): Promise<PostDisplayDTO> {
    const newPost = new Posst();
    newPost.title = post.title;
    newPost.content = post.content;
    newPost.user = Promise.resolve(user);
    const createdPost = await this.postRepository.save(newPost);
    return await this.transformService.transformPost(createdPost);
  }

  async checkAuthor(postAuthor: User, requestUser: User): Promise<void> {
    if (requestUser.role === UserRole.MEMBER && requestUser.id !== postAuthor.id) {
      throw new BlockedAccessException('A user can only update or delete their own posts');
    }
  }
  async updatePostById(post: PostAddAndUpdateDTO, id: string): Promise<{ message: string }> {
    const postToBeUpdated = { ...post, id: Number(id) };
    // return await this.postRepository.update(postToBeUpdated.id, postToBeUpdated);
    await this.postRepository.update(postToBeUpdated.id, postToBeUpdated);
    return { message: 'Update successful!'};
  }

  async deletePostById(postForDeletion: Posst): Promise<{ message: string }> {
    const updatedField = { isDeleted: true };
    // return await this.postRepository.update(postForDeletion.id, updatedField);
    await this.postRepository.update(postForDeletion.id, updatedField);
    return { message: 'Delete successful!'};
  }

  // async flagPostById(flaggedPost: Posst): Promise<UpdateResult> {
  //   const updatedField = { isFlagged: true };
  //   return await this.postRepository.update(flaggedPost.id, updatedField);
  // }
  async flagPostById(flaggedPost: Posst): Promise<UpdateResult> {
    const updatedField = { isFlagged: !flaggedPost.isFlagged };
    return await this.postRepository.update(flaggedPost.id, updatedField);
  }

  async lockPostById(lockPost: Posst): Promise<UpdateResult> {
    const updatedField = { isLocked: !lockPost.isLocked };
    return await this.postRepository.update(lockPost.id, updatedField);
  }
}
