
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PostService } from './post.service';
import { PostController } from './post.controller';
import { Posst } from '../entities/post.entity';
import { CoreModule } from '../core/core.module';
import { AuthModule } from '../auth/auth.module';

@Module({
    imports: [TypeOrmModule.forFeature([Posst]), CoreModule, AuthModule],
    providers: [PostService],
    controllers: [PostController],
    exports: [PostService],
})
export class PostModule {}
