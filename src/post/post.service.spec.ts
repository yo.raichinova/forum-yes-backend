import { Test, TestingModule } from '@nestjs/testing';
import { PostService } from './post.service';
import { TransformService } from '../core/services/transform.service';
import { Posst } from '../entities/post.entity';
import { getRepositoryToken } from '@nestjs/typeorm';
import { PostNotFound, BlockedAccessException } from '../common/filters/post-system-exception';
import { User, UserRole } from '../entities/user.entity';

describe('PostService', () => {
    let service: PostService;

    const transformService = {
        transformPost() { },

        transformPostArray() { },
    };
    const postRepository = {
        find() { },

        findOne() { },

        save() { },

        update() { },

        delete() { },
    };

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            providers: [
                PostService,
                {
                    provide: TransformService,
                    useValue: transformService,
                },
                {
                    provide: getRepositoryToken(Posst),
                    useValue: postRepository,
                },
            ],
        }).compile();

        service = module.get<PostService>(PostService);
    });

    it('should be defined', () => {
        expect(service).toBeDefined();
    });
    describe('findPostById', () => {
        it('findPostById should return the correct result', async () => {
            const postRepoFindOneSpy = jest.spyOn(postRepository, 'findOne')
                .mockImplementation(() => Promise.resolve(['testPost']));

            const result = await service.findPostById('test');

            expect(result).toEqual(['testPost']);

            postRepoFindOneSpy.mockRestore();
        });
        it('findPostById should call findOne once', async () => {

            const postRepoFindOneSpy = jest.spyOn(postRepository, 'findOne')
                .mockImplementation(() => Promise.resolve(['testPost']));

            await service.findPostById('test');

            expect(postRepoFindOneSpy).toBeCalledTimes(1);

            postRepoFindOneSpy.mockRestore();
        });
        it('findPostById should throw if no post was found', async () => {

            const postRepoFindOneSpy = jest.spyOn(postRepository, 'findOne').mockImplementation(() => undefined);

            try {
                await service.findPostById('test');
            } catch (err) {
                expect(err).toBeInstanceOf(PostNotFound);
            }
            postRepoFindOneSpy.mockRestore();
        });
    });
    describe('transformPostFordisplay', () => {
        it('transformPostForDisplay should return the correct result', async () => {

            const fakePost = new Posst();

            const fakeTransformPost = jest.spyOn(transformService, 'transformPost')
                .mockImplementation(() => Promise.resolve(['transformedParam']));
            const result = await service.transformPostForDisplay(fakePost);

            expect(result).toEqual(['transformedParam']);

            fakeTransformPost.mockRestore();
        });
        it('transformPostForDisplay should call transformPostForDisplay once with correct parameter', async () => {
            const fakePost = new Posst();

            const fakeTransformPost = jest.spyOn(transformService, 'transformPost')
                .mockImplementation(() => Promise.resolve(['transformedParam']));

            await service.transformPostForDisplay(fakePost);

            expect(fakeTransformPost).toBeCalledTimes(1);
            expect(fakeTransformPost).toBeCalledWith(fakePost);

            fakeTransformPost.mockRestore();
        });
    });
    describe('add method', () => {
        it('should return correct result', async () => {
            const fakePostDTO = {
                title: 'title',
                content: 'content',
            };
            const fakeUser = new User();
            const fakePost = new Posst();

            const fakeSave = jest.spyOn(postRepository, 'save')
                .mockImplementation(() => Promise.resolve(fakePost));
            const fakeTransformPost = jest.spyOn(transformService, 'transformPost')
                .mockImplementation(() => Promise.resolve(['transformedParam']));

            const result = await service.add(fakePostDTO, fakeUser);
            expect(result).toEqual(['transformedParam']);

            fakeSave.mockRestore();
            fakeTransformPost.mockRestore();
        });
        it('should call the postRepository.save method once with correct parameter', async () => {
            const fakePostDTO = {
                title: 'title',
                content: 'content',
            };
            const fakeUser = new User();

            const fakeSaveArgument = { ...fakePostDTO, user: Promise.resolve(fakeUser) };
            const fakeSave = jest.spyOn(postRepository, 'save');

            await service.add(fakePostDTO, fakeUser);

            expect(fakeSave).toBeCalledWith(fakeSaveArgument);
            expect(fakeSave).toBeCalledTimes(1);

            fakeSave.mockRestore();
        });
        it('should call the transformService.transformPost method once with correct parameter', async () => {
            const fakePostDTO = {
                title: 'title',
                content: 'content',
            };
            const fakeUser = new User();
            const fakePost = new Posst();

            const fakeSave = jest.spyOn(postRepository, 'save')
                .mockImplementation(() => Promise.resolve(fakePost));
            const fakeTransform = jest.spyOn(transformService, 'transformPost')
                .mockImplementation(() => Promise.resolve(['transformedParam']));;

            await service.add(fakePostDTO, fakeUser);

            expect(fakeTransform).toBeCalledWith(fakePost);
            expect(fakeSave).toBeCalledTimes(1);

            fakeSave.mockRestore();
            fakeTransform.mockRestore();
        });

    });
    describe('checkAuthor method', () => {
        it('should throw when post author and request user are not the same and when request user role is only a member', async () => {
            const author = new User();
            author.id = 3;
            const requestUser = new User();
            requestUser.id = 4;
            requestUser.role = UserRole.MEMBER;
            try {
                await service.checkAuthor(author, requestUser);
            } catch (err) {
                expect(err).toBeInstanceOf(BlockedAccessException);
            }
        });
        it('should not throw when post author and request user are the same person and/or when request user role is admin', async () => {
            const author = new User();
            author.id = 3;
            const requestUser = new User();
            requestUser.id = 3;
            requestUser.role = UserRole.MEMBER;

            const result = await service.checkAuthor(author, requestUser);
            expect(result).toBe(undefined);
        });
    });
    describe('updatePostById method', () => {
        it('should return correct result', async () => {
            const fakeId = '3';
            const fakePostDTO = {
                title: 'title',
                content: 'content',
            };
            const fakeUpdate = jest.spyOn(postRepository, 'update')
                .mockImplementation(() => Promise.resolve(['update successful']));

            const result = await service.updatePostById(fakePostDTO, fakeId);

            expect(result).toEqual(['update successful']);

            fakeUpdate.mockRestore();
        });
        it('should call postRepository.update method once with correct parameters', async () => {
            const fakeId = '3';
            const fakePostDTO = {
                title: 'title',
                content: 'content',
            };
            const fakePostToBeUpdated = { ...fakePostDTO, id: +fakeId };
            const fakeUpdate = jest.spyOn(postRepository, 'update')
                .mockImplementation(() => Promise.resolve(['update successful']));

            await service.updatePostById(fakePostDTO, fakeId);

            expect(fakeUpdate).toBeCalledTimes(1);
            expect(fakeUpdate).toBeCalledWith(+fakeId, fakePostToBeUpdated);

            fakeUpdate.mockRestore();
        });
    });
    describe('deletePostById method', () => {
        it('should return correct result', async () => {
            const fakePost = new Posst();
            const fakeDelete = jest.spyOn(postRepository, 'update')
                .mockImplementation(() => Promise.resolve(['delete successful']));

            const result = await service.deletePostById(fakePost);
            expect(result).toEqual(['delete successful']);

            fakeDelete.mockRestore();
        });
        it('should call postRepository.update method once with correct parameters', async () => {
            const fakePost = new Posst();
            fakePost.id = 3;
            const fakeDelete = jest.spyOn(postRepository, 'update')
                .mockImplementation(() => Promise.resolve(['delete successful']));

            await service.deletePostById(fakePost);

            expect(fakeDelete).toBeCalledTimes(1);
            expect(fakeDelete).toBeCalledWith(fakePost.id, { isDeleted: true });

            fakeDelete.mockRestore();
        });
    });
    describe('findAllByQuery method', () => {
        it('should return correct result when query is an empty object', async () => {
            const fakeQuery = {};
            const fakeFind = jest.spyOn(postRepository, 'find')
                .mockImplementation(() => Promise.resolve(['find successful']));
            const fakeTransformArray = jest.spyOn(transformService, 'transformPostArray')
                .mockImplementation(() => Promise.resolve(['transformed result']));

            const result = await service.findAllByQuery(fakeQuery);
            expect(result).toEqual(['transformed result']);

            fakeFind.mockRestore();
            fakeTransformArray.mockRestore();
        });

        it('should return correct result when query contains search phrase', async () => {
            const fakeQuery = { text: 'wow' };
            const fakeFind = jest.spyOn(postRepository, 'find')
                .mockImplementation(() => Promise.resolve(['find successful']));
            const fakeTransformArray = jest.spyOn(transformService, 'transformPostArray')
                .mockImplementation(() => Promise.resolve(['transformed result']));

            const result = await service.findAllByQuery(fakeQuery);
            expect(result).toEqual(['transformed result']);

            fakeFind.mockRestore();
            fakeTransformArray.mockRestore();
        });
        it('should throw when query contains search phrase not found in any post title/content', async () => {
            const fakeQuery = { text: 'wow' };
            const fakeFind = jest.spyOn(postRepository, 'find')
                .mockImplementation(() => Promise.resolve([]));
            const fakeTransformArray = jest.spyOn(transformService, 'transformPostArray')
                .mockImplementation(() => Promise.resolve(['transformed result']));

            try {
                await service.findAllByQuery(fakeQuery);
            } catch (err) {
                expect(err).toBeInstanceOf(PostNotFound);
            }

            fakeFind.mockRestore();
            fakeTransformArray.mockRestore();
        });
    });
    describe('flagPostById method', () => {
        it('should return correct result', async () => {
            const fakePost = new Posst();
            const fakeFlag = jest.spyOn(postRepository, 'update')
                .mockImplementation(() => Promise.resolve(['flag successful']));

            const result = await service.flagPostById(fakePost);
            expect(result).toEqual(['flag successful']);

            fakeFlag.mockRestore();
        });
        it('should call postRepository.update method once with correct parameters', async () => {
            const fakePost = new Posst();
            fakePost.id = 3;
            const fakeFlag = jest.spyOn(postRepository, 'update')
                .mockImplementation(() => Promise.resolve(['flag successful']));

            await service.flagPostById(fakePost);

            expect(fakeFlag).toBeCalledTimes(1);
            expect(fakeFlag).toBeCalledWith(fakePost.id, { isFlagged: true });

            fakeFlag.mockRestore();
        });
    });
    describe('lockPostById method', () => {
        it('should return correct result', async () => {
            const fakePost = new Posst();
            const fakeLock = jest.spyOn(postRepository, 'update')
                .mockImplementation(() => Promise.resolve(['lock successful']));

            const result = await service.lockPostById(fakePost);
            expect(result).toEqual(['lock successful']);

            fakeLock.mockRestore();
        });
        it('should call postRepository.update method once with correct parameters', async () => {
            const fakePost = new Posst();

            fakePost.id = 3;
            const fakeLock = jest.spyOn(postRepository, 'update')
                .mockImplementation(() => Promise.resolve(['lock successful']));

            await service.lockPostById(fakePost);

            expect(fakeLock).toBeCalledTimes(1);
            expect(fakeLock).toBeCalledWith(fakePost.id, { isLocked: !fakePost.isLocked });

            fakeLock.mockRestore();
        });
    });
});
