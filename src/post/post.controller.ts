import 'reflect-metadata';
import {
    Controller,
    Get,
    Post,
    Body,
    UseGuards,
    Req,
    Param,
    Put,
    HttpStatus,
    Delete,
    ValidationPipe,
    UseFilters,
    HttpCode,
    Query,
} from '@nestjs/common';
import { PostService } from './post.service';
import { AuthGuard } from '@nestjs/passport';
import { PostAddAndUpdateDTO } from '../models/post-add-and-update-dto';
import { PostDisplayDTO } from '../models/post-display-dto';
import { StatusGuard } from '../guards/status.guard';
import { NotFoundFilter } from '../common/filters/not-found.filter';
import { BlockedAccessFilter } from '../common/filters/blocked-access.filter';
import { UpdateResult } from 'typeorm';
import { PostQueryDTO } from '../models/post-query-dto';
import { LikesService } from '../core/services/likes.service';
import { LockGuard } from '../guards/lock.guard';
import { RolesGuard } from '../guards/roles.guard';
import { Roles } from '../decorators/roles.decorator';

@Controller('posts')
@UseGuards(AuthGuard('jwt'))
export class PostController {

    constructor(
        private readonly postService: PostService,
        private readonly likesService: LikesService,
    ) { }

    @Get()
    @HttpCode(HttpStatus.OK)
    @UseFilters(NotFoundFilter)
    async all(@Query() query: PostQueryDTO): Promise<PostDisplayDTO[]> {
        return await this.postService.findAllByQuery(query);
    }

    @Get(':postId')
    @HttpCode(HttpStatus.OK)
    @UseFilters(NotFoundFilter)
    async getPostById(@Param('postId') id: string): Promise<any> {
        const postFound = await this.postService.findPostById(id);

        return await this.postService.transformPostForDisplay(postFound);

    }

    @Post()
    @HttpCode(HttpStatus.CREATED)
    @UseGuards(StatusGuard)
    async add(
        @Body(new ValidationPipe({ whitelist: true, transform: true })) post: PostAddAndUpdateDTO,
        @Req() request: any,
    ): Promise<PostDisplayDTO> {

        const user = request.user;
        return await this.postService.add(post, user);
    }

    @Put(':postId')
    @HttpCode(HttpStatus.OK)
    @UseFilters(NotFoundFilter, BlockedAccessFilter)
    @UseGuards(StatusGuard, LockGuard)
    async update(
        @Param('postId') id: string, @Body(new ValidationPipe({ whitelist: true, transform: true })) postData: PostAddAndUpdateDTO,
        @Req() request: any,
    ): Promise<{ message: string }> {
        const postFound = await this.postService.findPostById(id);

        const postAuthor = await postFound.user;
        await this.postService.checkAuthor(postAuthor, request.user);

        return await this.postService.updatePostById(postData, id);
    }

    @Put(':postId/votes')
    @HttpCode(HttpStatus.OK)
    @UseFilters(NotFoundFilter, BlockedAccessFilter)
    @UseGuards(StatusGuard, LockGuard)
    async vote(@Param('postId') id: string, @Req() request: any): Promise<PostDisplayDTO> {
        const postFound = await this.postService.findPostById(id);
        await this.likesService.prepareForVoting(postFound, request.user, postFound.likedBy, postFound.dislikedBy);

        const likedPost = await this.likesService.upvotePost(postFound, request.user); 
        return this.postService.transformPostForDisplay(likedPost);
    }

    @Put(':postId/downvotes')
    @HttpCode(HttpStatus.OK)
    @UseFilters(NotFoundFilter, BlockedAccessFilter)
    @UseGuards(StatusGuard, LockGuard)
    async downvote(@Param('postId') id: string, @Req() request: any): Promise<PostDisplayDTO> {
        const postFound = await this.postService.findPostById(id);
        await this.likesService.prepareForVoting(postFound, request.user, postFound.dislikedBy, postFound.likedBy);

        const dislikedPost = await this.likesService.downvotePost(postFound, request.user);
        return this.postService.transformPostForDisplay(dislikedPost);
    }

    @Delete(':postId')
    @HttpCode(HttpStatus.OK)
    @UseFilters(NotFoundFilter)
    @UseGuards(StatusGuard, LockGuard)
    async delete(@Param('postId') id: string, @Req() request: any): Promise<{message: string}> {
        const postForDeletion = await this.postService.findPostById(id);
        const postAuthor = await postForDeletion.user;
        await this.postService.checkAuthor(postAuthor, request.user);

        return await this.postService.deletePostById(postForDeletion);
    }

    @Put(':postId/flag')
    @HttpCode(HttpStatus.OK)
    @UseFilters(NotFoundFilter)
    @UseGuards(StatusGuard, LockGuard)
    async flag(@Param('postId') id: string): Promise<any> {
        const postForFlagging = await this.postService.findPostById(id);
        return await this.postService.flagPostById(postForFlagging);
    }

    @Put(':postId/lock')
    @HttpCode(HttpStatus.OK)
    @UseFilters(NotFoundFilter)
    @Roles('admin')
    @UseGuards(RolesGuard)
    async lock(@Param('postId') id: string): Promise<UpdateResult> {
        const postForLocking = await this.postService.findPostById(id);
        return await this.postService.lockPostById(postForLocking);
    }
}
