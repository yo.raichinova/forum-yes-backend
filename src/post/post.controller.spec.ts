import { Test, TestingModule } from '@nestjs/testing';
import { PostController } from './post.controller';
import { PostService } from './post.service';
import { LikesService } from '../core/services/likes.service';
import { PostQueryDTO } from '../models/post-query-dto';
import { User } from '../entities/user.entity';
import { Posst } from '../entities/post.entity';
import { BlockedAccessException, PostNotFound } from '../common/filters/post-system-exception';

describe('PostController', () => {
    let controller: PostController;

    const postService = {
        findAllByQuery() {},

        findPostById() {},

        transformPostForDisplay() {},

        add() {},

        checkAuthor() {},

        updatePostById() {},

        deletePostById() {},

        flagPostById() {},

        lockPostById() {}

    };

    const likesService = {
        prepareForVoting() {},

        upvotePost() {},

        downvotePost() {}
    };

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            controllers: [PostController],
            providers: [
                {
                    provide: PostService,
                    useValue: postService,
                },
                {
                    provide: LikesService,
                    useValue: likesService,
                },
            ],
        }).compile();

        controller = module.get<PostController>(PostController);
    });

    it('should be defined', () => {
        expect(controller).toBeDefined();
    });
    describe('all method', () =>{
        it('all method should call findAllByQuery method once with correct query', async () => {
            const fakeQuery: PostQueryDTO = { text: 'text '};
            const fakeFindPostsByQuery = jest.spyOn(postService, 'findAllByQuery')
            .mockImplementation(() => Promise.resolve(['testPost']));

            await controller.all(fakeQuery);

            expect(fakeFindPostsByQuery).toBeCalledTimes(1);
            expect(fakeFindPostsByQuery).toHaveBeenCalledWith(fakeQuery);

            fakeFindPostsByQuery.mockRestore();
        });
        it('should return the correct result from findAllByQuery method', async () => {
            const fakeQuery: PostQueryDTO = { text: 'text ' };
            const fakeFindPostsByQuery = jest.spyOn(postService, 'findAllByQuery')
                .mockImplementation(() => Promise.resolve(['testPost']));

            const result = await controller.all(fakeQuery);

            expect(result).toEqual(['testPost']);

            fakeFindPostsByQuery.mockRestore();
        });
    });
    describe('getPostById method', () => {
        it('getPostById should call findPostById method once with correct id', async () => {
            const fakeId: string = '3';

            const fakePost = new Posst();
            const fakeFindPostById = jest.spyOn(postService, 'findPostById')
                .mockImplementation(() => Promise.resolve(fakePost));
            const fakeTransformPostForDisplay = jest.spyOn(postService, 'transformPostForDisplay')
                .mockImplementation(() => Promise.resolve(['result']));

            await controller.getPostById(fakeId);

            expect(fakeFindPostById).toBeCalledTimes(1);
            expect(fakeFindPostById).toHaveBeenCalledWith(fakeId);

            fakeFindPostById.mockRestore();
            fakeTransformPostForDisplay.mockRestore();
        });
        it('getPostById should call the transformPostForDisplay method once with correct post', async () => {
            const fakeId: string = '3';

            const fakePost = new Posst();
            const fakeFindPostById = jest.spyOn(postService, 'findPostById')
                .mockImplementation(() => Promise.resolve(fakePost));
            const fakeTransformPostForDisplay = jest.spyOn(postService, 'transformPostForDisplay')
                .mockImplementation(() => Promise.resolve(['result']));

            await controller.getPostById(fakeId);

            expect(fakeTransformPostForDisplay).toBeCalledTimes(1);
            expect(fakeTransformPostForDisplay).toHaveBeenCalledWith(fakePost);

            fakeFindPostById.mockRestore();
            fakeTransformPostForDisplay.mockRestore();
        });

        it('getPostById should return the correct result from the transformPostForDisplay method', async () => {
            const fakeId: string = '3';

            const fakePost = new Posst();
            const fakeFindPostById = jest.spyOn(postService, 'findPostById')
                .mockImplementation(() => Promise.resolve(fakePost));
            const fakeTransformPostForDisplay = jest.spyOn(postService, 'transformPostForDisplay')
                .mockImplementation(() => Promise.resolve(['result']));
            const result = await controller.getPostById(fakeId);
            expect(result).toEqual(['result']);
            fakeFindPostById.mockRestore();
            fakeTransformPostForDisplay.mockRestore();
        });
    });

    describe('add method', () => {
        it('should call the postService.add method once with correct parameters', async () => {
            const fakeUser = new User();
            const fakePostDTO = {
                title: 'title',
                content: 'content',
            };
            const fakeRequest = {
                user: fakeUser,
            }
            const fakeAdd = jest.spyOn(postService, 'add')
                .mockImplementation(() => Promise.resolve(['result']));

            await controller.add(fakePostDTO, fakeRequest);
            expect(fakeAdd).toBeCalledTimes(1);
            expect(fakeAdd).toHaveBeenCalledWith(fakePostDTO, fakeUser);

            fakeAdd.mockRestore();

        });
        it('should return correct result from the postService.add method', async () => {
            const fakeUser = new User();
            const fakePostDTO = {
                title: 'title',
                content: 'content',
            };
            const fakeRequest = {
                user: fakeUser,
            }
            const fakeAdd = jest.spyOn(postService, 'add')
                .mockImplementation(() => Promise.resolve(['result']));

            const result = await controller.add(fakePostDTO, fakeRequest);
            expect(result).toEqual(['result']);

            fakeAdd.mockRestore();

        });
    });
    describe('update method', () => {
        it('should call the postService.findPostById method once with correct id', async () => {
            const fakeUser = new User();
            const fakePostDTO = {
                title: 'title',
                content: 'content',
            };
            const fakeRequest = {
                user: fakeUser,
            }
            const fakeId: string = '3';

            const fakePost = new Posst();
            const fakeFindPostById = jest.spyOn(postService, 'findPostById')
                .mockImplementation(() => Promise.resolve(fakePost));
            const fakeCheckAuthor = jest.spyOn(postService, 'checkAuthor')
                .mockImplementation(() => Promise.resolve(true));
            const fakeUpdatePostById = jest.spyOn(postService, 'updatePostById')
                .mockImplementation(() => Promise.resolve(['result']));

            await controller.update(fakeId, fakePostDTO, fakeRequest);

            expect(fakeFindPostById).toBeCalledTimes(1);
            expect(fakeFindPostById).toHaveBeenCalledWith(fakeId);

            fakeFindPostById.mockRestore();
            fakeCheckAuthor.mockRestore();
            fakeUpdatePostById.mockRestore();

        });
        it('should call the postService.checkAuthor method once with correct parameters', async () => {
            const fakeUser = new User();
            const fakePostDTO = {
                title: 'title',
                content: 'content',
            };
            const fakeRequest = {
                user: fakeUser,
            }
            const fakeId: string = '3';

            const fakePost = new Posst();
            const fakeFindPostById = jest.spyOn(postService, 'findPostById')
                .mockImplementation(() => Promise.resolve(fakePost));
            const fakeCheckAuthor = jest.spyOn(postService, 'checkAuthor')
                .mockImplementation(() => Promise.resolve(true));
            const fakeUpdatePostById = jest.spyOn(postService, 'updatePostById')
                .mockImplementation(() => Promise.resolve(['result']));

            await controller.update(fakeId, fakePostDTO, fakeRequest);

            expect(fakeCheckAuthor).toBeCalledTimes(1);
            expect(fakeCheckAuthor).toHaveBeenCalledWith(await fakePost.user, fakeUser);

            fakeFindPostById.mockRestore();
            fakeCheckAuthor.mockRestore();
            fakeUpdatePostById.mockRestore();

        });
        it('should call the postService.updatePostById method once with correct parameters', async () => {
            const fakeUser = new User();
            const fakePostDTO = {
                title: 'title',
                content: 'content',
            };
            const fakeRequest = {
                user: fakeUser,
            }
            const fakeId: string = '3';

            const fakePost = new Posst();
            const fakeFindPostById = jest.spyOn(postService, 'findPostById')
                .mockImplementation(() => Promise.resolve(fakePost));
            const fakeCheckAuthor = jest.spyOn(postService, 'checkAuthor')
                .mockImplementation(() => Promise.resolve(true));
            const fakeUpdatePostById = jest.spyOn(postService, 'updatePostById')
                .mockImplementation(() => Promise.resolve(['result']));

            await controller.update(fakeId, fakePostDTO, fakeRequest);

            expect(fakeUpdatePostById).toBeCalledTimes(1);
            expect(fakeUpdatePostById).toHaveBeenCalledWith(fakePostDTO, fakeId);

            fakeFindPostById.mockRestore();
            fakeCheckAuthor.mockRestore();
            fakeUpdatePostById.mockRestore();
        });
        it('should return correct result', async () => {
            const fakeUser = new User();
            const fakePostDTO = {
                title: 'title',
                content: 'content',
            };
            const fakeRequest = {
                user: fakeUser,
            }
            const fakeId: string = '3';

            const fakePost = new Posst();
            const fakeFindPostById = jest.spyOn(postService, 'findPostById')
                .mockImplementation(() => Promise.resolve(fakePost));
            const fakeCheckAuthor = jest.spyOn(postService, 'checkAuthor')
                .mockImplementation(() => Promise.resolve(true));
            const fakeUpdatePostById = jest.spyOn(postService, 'updatePostById')
                .mockImplementation(() => Promise.resolve(['result']));

            const result = await controller.update(fakeId, fakePostDTO, fakeRequest);

            expect(result).toEqual(['result']);

            fakeFindPostById.mockRestore();
            fakeCheckAuthor.mockRestore();
            fakeUpdatePostById.mockRestore();
        });
        it('should return correct result', async () => {
            const fakeUser = new User();
            const fakePostDTO = {
                title: 'title',
                content: 'content',
            };
            const fakeRequest = {
                user: fakeUser,
            }
            const fakeId: string = '3';

            const fakePost = new Posst();
            const fakeFindPostById = jest.spyOn(postService, 'findPostById')
                .mockImplementation(() => Promise.resolve(fakePost));
            const fakeCheckAuthor = jest.spyOn(postService, 'checkAuthor')

            const fakeUpdatePostById = jest.spyOn(postService, 'updatePostById')
                .mockImplementation(() => Promise.resolve(['result']));

            try {
                await controller.update(fakeId, fakePostDTO, fakeRequest);
            } catch (err) {
                expect(err).toBeInstanceOf(BlockedAccessException);
            }

            fakeFindPostById.mockRestore();
            fakeCheckAuthor.mockRestore();
            fakeUpdatePostById.mockRestore();
        });
        it('should not call checkAuthor if no post was found by findPostById method', async () => {
            const fakeUser = new User();
            const fakeRequest = {
                user: fakeUser,
            };
            const fakePostDTO = {
                title: 'title',
                content: 'content',
            };
            const fakeId: string = '3';

            const fakeFindPostById = jest.spyOn(postService, 'findPostById')
                .mockImplementation(() => { throw new PostNotFound('Post with such id does not exist') });
            const fakeCheckAuthor = jest.spyOn(postService, 'checkAuthor');

            const fakeUpdatePostById = jest.spyOn(postService, 'updatePostById')
                .mockImplementation(() => Promise.resolve(['result']));
            try {
                await controller.update(fakeId, fakePostDTO, fakeRequest);
            } catch (err) {}

            expect(fakeCheckAuthor).not.toHaveBeenCalled();

            fakeFindPostById.mockRestore();
            fakeCheckAuthor.mockRestore();
            fakeUpdatePostById.mockRestore();
        });
        it('should not call updatePostById if post author and request user are different people', async () => {
            const fakeUser = new User();
            const fakeRequest = {
                user: fakeUser,
            };
            const fakePostDTO = {
                title: 'title',
                content: 'content',
            };
            const fakeId: string = '3';

            const fakePost = new Posst();
            const fakeFindPostById = jest.spyOn(postService, 'findPostById')
                .mockImplementation(() => fakePost);
            const fakeCheckAuthor = jest.spyOn(postService, 'checkAuthor')
                .mockImplementation(() => {throw new BlockedAccessException()});
            const fakeUpdatePostById = jest.spyOn(postService, 'updatePostById')
                .mockImplementation(() => Promise.resolve(['result']));
            try {
                await controller.update(fakeId, fakePostDTO, fakeRequest);
            } catch (err) {}

            expect(fakeUpdatePostById).not.toHaveBeenCalled();

            fakeFindPostById.mockRestore();
            fakeCheckAuthor.mockRestore();
            fakeUpdatePostById.mockRestore();
        });
    });
    describe('delete method', () => {
        it('should call the postService.findPostById method once with correct id', async () => {
            const fakeUser = new User();
            const fakeRequest = {
                user: fakeUser,
            };
            const fakeId: string = '3';

            const fakePost = new Posst();
            const fakeFindPostById = jest.spyOn(postService, 'findPostById')
                .mockImplementation(() => Promise.resolve(fakePost));
            const fakeCheckAuthor = jest.spyOn(postService, 'checkAuthor')
                .mockImplementation(() => Promise.resolve(true));
            const fakeDeletePostById = jest.spyOn(postService, 'deletePostById')
                .mockImplementation(() => Promise.resolve(['result']));

            await controller.delete(fakeId, fakeRequest);

            expect(fakeFindPostById).toBeCalledTimes(1);
            expect(fakeFindPostById).toHaveBeenCalledWith(fakeId);

            fakeFindPostById.mockRestore();
            fakeCheckAuthor.mockRestore();
            fakeDeletePostById.mockRestore();

        });
        it('should call the postService.checkAuthor method once with correct parameters', async () => {
            const fakeUser = new User();
            const fakeRequest = {
                user: fakeUser,
            };
            const fakeId: string = '3';

            const fakePost = new Posst();
            const fakeFindPostById = jest.spyOn(postService, 'findPostById')
                .mockImplementation(() => Promise.resolve(fakePost));
            const fakeCheckAuthor = jest.spyOn(postService, 'checkAuthor')
                .mockImplementation(() => Promise.resolve(true));
            const fakeDeletePostById = jest.spyOn(postService, 'deletePostById')
                .mockImplementation(() => Promise.resolve(['result']));

            await controller.delete(fakeId, fakeRequest);

            expect(fakeCheckAuthor).toBeCalledTimes(1);
            expect(fakeCheckAuthor).toHaveBeenCalledWith(await fakePost.user, fakeUser);

            fakeFindPostById.mockRestore();
            fakeCheckAuthor.mockRestore();
            fakeDeletePostById.mockRestore();

        });
        it('should call the postService.deletePostById method once with correct parameters', async () => {
            const fakeUser = new User();
            const fakeRequest = {
                user: fakeUser,
            };
            const fakeId: string = '3';

            const fakePost = new Posst();
            const fakeFindPostById = jest.spyOn(postService, 'findPostById')
                .mockImplementation(() => Promise.resolve(fakePost));
            const fakeCheckAuthor = jest.spyOn(postService, 'checkAuthor')
                .mockImplementation(() => Promise.resolve(true));
            const fakeDeletePostById = jest.spyOn(postService, 'deletePostById')
                .mockImplementation(() => Promise.resolve(['result']));

            await controller.delete(fakeId, fakeRequest);

            expect(fakeDeletePostById).toBeCalledTimes(1);
            expect(fakeDeletePostById).toHaveBeenCalledWith(fakePost);

            fakeFindPostById.mockRestore();
            fakeCheckAuthor.mockRestore();
            fakeDeletePostById.mockRestore();
        });
        it('should return correct result', async () => {
            const fakeUser = new User();

            const fakeRequest = {
                user: fakeUser,
            }
            const fakeId: string = '3';

            const fakePost = new Posst();
            const fakeFindPostById = jest.spyOn(postService, 'findPostById')
                .mockImplementation(() => Promise.resolve(fakePost));
            const fakeCheckAuthor = jest.spyOn(postService, 'checkAuthor')
                .mockImplementation(() => Promise.resolve(true));
            const fakeDeletePostById = jest.spyOn(postService, 'deletePostById')
                .mockImplementation(() => Promise.resolve(['result']));

            const result = await controller.delete(fakeId, fakeRequest);

            expect(result).toEqual(['result']);

            fakeFindPostById.mockRestore();
            fakeCheckAuthor.mockRestore();
            fakeDeletePostById.mockRestore();
        });
        it('should throw if author and request user are different people', async () => {
            const fakeUser = new User();
            const fakeRequest = {
                user: fakeUser,
            };
            const fakeId: string = '3';

            const fakePost = new Posst();
            const fakeFindPostById = jest.spyOn(postService, 'findPostById')
                .mockImplementation(() => Promise.resolve(fakePost));
            const fakeCheckAuthor = jest.spyOn(postService, 'checkAuthor')

            const fakeDeletePostById = jest.spyOn(postService, 'deletePostById')
                .mockImplementation(() => Promise.resolve(['result']));

            try {
                await controller.delete(fakeId, fakeRequest);
            } catch (err) {
                expect(err).toBeInstanceOf(BlockedAccessException);
            }
            fakeFindPostById.mockRestore();
            fakeCheckAuthor.mockRestore();
            fakeDeletePostById.mockRestore();
        });
        it('should not call checkAuthor if no post was found by findPostById method', async () => {
            const fakeUser = new User();
            const fakeRequest = {
                user: fakeUser,
            };
            const fakeId: string = '3';

            const fakeFindPostById = jest.spyOn(postService, 'findPostById')
                .mockImplementation(() => {throw new PostNotFound('Post with such id does not exist')});
            const fakeCheckAuthor = jest.spyOn(postService, 'checkAuthor');

            const fakeDeletePostById = jest.spyOn(postService, 'deletePostById')
                .mockImplementation(() => Promise.resolve(['result']));
            try {
                await controller.delete(fakeId, fakeRequest);
            } catch (err) {}

            expect(fakeCheckAuthor).not.toHaveBeenCalled();

            fakeFindPostById.mockRestore();
            fakeCheckAuthor.mockRestore();
            fakeDeletePostById.mockRestore();
        });
        it('should not call deletePostById if no post was found by findPostById method', async () => {
            const fakeUser = new User();
            const fakeRequest = {
                user: fakeUser,
            };
            const fakeId: string = '3';

            const fakeFindPostById = jest.spyOn(postService, 'findPostById')
                .mockImplementation(() => { throw new PostNotFound('Post with such id does not exist') });
            const fakeCheckAuthor = jest.spyOn(postService, 'checkAuthor');

            const fakeDeletePostById = jest.spyOn(postService, 'deletePostById')
                .mockImplementation(() => Promise.resolve(['result']));
            try {
                await controller.delete(fakeId, fakeRequest);
            } catch (err) { }

            expect(fakeDeletePostById).not.toHaveBeenCalled();

            fakeFindPostById.mockRestore();
            fakeCheckAuthor.mockRestore();
            fakeDeletePostById.mockRestore();
        });
        it('should not call deletePostById if post author and request user are different people', async () => {
            const fakeUser = new User();
            const fakeRequest = {
                user: fakeUser,
            };
            const fakeId: string = '3';

            const fakePost = new Posst();

            const fakeFindPostById = jest.spyOn(postService, 'findPostById')
                .mockImplementation(() => fakePost);
            const fakeCheckAuthor = jest.spyOn(postService, 'checkAuthor')
                .mockImplementation(() => {throw new BlockedAccessException()});
            const fakeDeletePostById = jest.spyOn(postService, 'deletePostById')
                .mockImplementation(() => Promise.resolve(['result']));
            try {
                await controller.delete(fakeId, fakeRequest);
            } catch (err) {}

            expect(fakeDeletePostById).not.toHaveBeenCalled();

            fakeFindPostById.mockRestore();
            fakeCheckAuthor.mockRestore();
            fakeDeletePostById.mockRestore();
        });
    });
    describe('vote method', () => {
        it('should call the postService.findPostById method once with correct id', async () => {
            const fakeUser = new User();
            const fakeRequest = {
                user: fakeUser,
            };
            const fakeId: string = '3';

            const fakePost = new Posst();
            const fakeLikedPost = { ...fakePost, likedBy: Promise.resolve([fakeUser]) };

            const fakeFindPostById = jest.spyOn(postService, 'findPostById')
                .mockImplementation(() => Promise.resolve(fakePost));
            const fakePrepareForVoting = jest.spyOn(likesService, 'prepareForVoting')
                .mockImplementation(() => Promise.resolve(true));
            const fakeUpvotePost = jest.spyOn(likesService, 'upvotePost')
                .mockImplementation(() => Promise.resolve(fakeLikedPost));
            const fakeTransformPost = jest.spyOn(postService, 'transformPostForDisplay')
                .mockImplementation(() => Promise.resolve(['result']));

            await controller.vote(fakeId, fakeRequest);

            expect(fakeFindPostById).toBeCalledTimes(1);
            expect(fakeFindPostById).toHaveBeenCalledWith(fakeId);

            fakeFindPostById.mockRestore();
            fakePrepareForVoting.mockRestore();
            fakeUpvotePost.mockRestore();
            fakeTransformPost.mockRestore();

        });
        it('should call the likesService.prepareForVoting method once when post is found', async () => {
            const fakeUser = new User();
            const fakeRequest = {
                user: fakeUser,
            };
            const fakeId: string = '3';

            const fakePost = new Posst();
            const fakeLikedPost = { ...fakePost, likedBy: Promise.resolve([fakeUser]) };
            const fakeFindPostById = jest.spyOn(postService, 'findPostById')
                .mockImplementation(() => Promise.resolve(fakePost));
            const fakePrepareForVoting = jest.spyOn(likesService, 'prepareForVoting')
                .mockImplementation(() => Promise.resolve(true));
            const fakeUpvotePost = jest.spyOn(likesService, 'upvotePost')
                .mockImplementation(() => Promise.resolve(fakeLikedPost));
            const fakeTransformPost = jest.spyOn(postService, 'transformPostForDisplay')
                .mockImplementation(() => Promise.resolve(['result']));

            await controller.vote(fakeId, fakeRequest);

            expect(fakePrepareForVoting).toBeCalledTimes(1);
 
            fakeFindPostById.mockRestore();
            fakePrepareForVoting.mockRestore();
            fakeUpvotePost.mockRestore();
            fakeTransformPost.mockRestore();

        });
        it('should not call the likesService.prepareForVoting method if post was not found by findPostById method', async () => {
            const fakeUser = new User();
            const fakeRequest = {
                user: fakeUser,
            };
            const fakeId: string = '3';

            const fakePost = new Posst();
            const fakeLikedPost = { ...fakePost, likedBy: Promise.resolve([fakeUser]) };
            const fakeFindPostById = jest.spyOn(postService, 'findPostById')
                .mockImplementation(() => {throw new PostNotFound()});
            const fakePrepareForVoting = jest.spyOn(likesService, 'prepareForVoting')
                .mockImplementation(() => Promise.resolve(true));
            const fakeUpvotePost = jest.spyOn(likesService, 'upvotePost')
                .mockImplementation(() => Promise.resolve(fakeLikedPost));
            const fakeTransformPost = jest.spyOn(postService, 'transformPostForDisplay')
                .mockImplementation(() => Promise.resolve(['result']));
            try {
                await controller.vote(fakeId, fakeRequest);
            } catch (err) {}

            expect(fakePrepareForVoting).not.toHaveBeenCalled();

            fakeFindPostById.mockRestore();
            fakePrepareForVoting.mockRestore();
            fakeUpvotePost.mockRestore();
            fakeTransformPost.mockRestore();
        });
        it('should call the likesService.upvotePost method once with correct parameters when post is found', async () => {
            const fakeUser = new User();
            const fakeRequest = {
                user: fakeUser,
            };
            const fakeId: string = '3';

            const fakePost = new Posst();
            const fakeLikedPost = { ...fakePost, likedBy: Promise.resolve([fakeUser]) };
            const fakeFindPostById = jest.spyOn(postService, 'findPostById')
                .mockImplementation(() => Promise.resolve(fakePost));
            const fakePrepareForVoting = jest.spyOn(likesService, 'prepareForVoting')
                .mockImplementation(() => Promise.resolve(true));
            const fakeUpvotePost = jest.spyOn(likesService, 'upvotePost')
                .mockImplementation(() => Promise.resolve(fakeLikedPost));
            const fakeTransformPost = jest.spyOn(postService, 'transformPostForDisplay')
                .mockImplementation(() => Promise.resolve(['result']));

            await controller.vote(fakeId, fakeRequest);

            expect(fakeUpvotePost).toBeCalledTimes(1);
            expect(fakeUpvotePost).toHaveBeenCalledWith(fakePost, fakeUser);

            fakeFindPostById.mockRestore();
            fakePrepareForVoting.mockRestore();
            fakeUpvotePost.mockRestore();
            fakeTransformPost.mockRestore();

        });
        it('should not call the likesService.upvotePost method if likesService.prepareForVoting method throws', async () => {
            const fakeUser = new User();
            const fakeRequest = {
                user: fakeUser,
            };
            const fakeId: string = '3';

            const fakePost = new Posst();
            const fakeLikedPost = { ...fakePost, likedBy: Promise.resolve([fakeUser]) };
            const fakeFindPostById = jest.spyOn(postService, 'findPostById')
                .mockImplementation(() => Promise.resolve(fakePost));
            const fakePrepareForVoting = jest.spyOn(likesService, 'prepareForVoting')
                .mockImplementation(() => {throw new BlockedAccessException()});
            const fakeUpvotePost = jest.spyOn(likesService, 'upvotePost')
                .mockImplementation(() => Promise.resolve(fakeLikedPost));
            const fakeTransformPost = jest.spyOn(postService, 'transformPostForDisplay')
                .mockImplementation(() => Promise.resolve(['result']));

            try {
                await controller.vote(fakeId, fakeRequest);
            } catch (err) {}

            expect(fakeUpvotePost).not.toHaveBeenCalled();

            fakeFindPostById.mockRestore();
            fakePrepareForVoting.mockRestore();
            fakeUpvotePost.mockRestore();
            fakeTransformPost.mockRestore();

        });
        it('should call the postService.transformPostForDisplay method if post is found and prepareForVoting method does not throw', async () => {
            const fakeUser = new User();
            const fakeRequest = {
                user: fakeUser,
            };
            const fakeId: string = '3';
            const fakePost = new Posst();
            const fakeLikedPost = {...fakePost, likedBy: Promise.resolve([fakeUser])};

            const fakeFindPostById = jest.spyOn(postService, 'findPostById')
                .mockImplementation(() => Promise.resolve(fakePost));
            const fakePrepareForVoting = jest.spyOn(likesService, 'prepareForVoting')
                .mockImplementation(() => Promise.resolve(true));
            const fakeUpvotePost = jest.spyOn(likesService, 'upvotePost')
                .mockImplementation(() => Promise.resolve(fakeLikedPost));
            const fakeTransformPost = jest.spyOn(postService, 'transformPostForDisplay')
                .mockImplementation(() => Promise.resolve(['result']));

            await controller.vote(fakeId, fakeRequest);

            expect(fakeTransformPost).toBeCalledTimes(1);
            expect(fakeTransformPost).toBeCalledWith(fakeLikedPost);

            fakeFindPostById.mockRestore();
            fakePrepareForVoting.mockRestore();
            fakeUpvotePost.mockRestore();
            fakeTransformPost.mockRestore();

        });
        it('should return correct result when post is found and voting is allowed by the likesService.prepareForVoting method', async () => {
            const fakeUser = new User();
            const fakeRequest = {
                user: fakeUser,
            };
            const fakeId: string = '3';
            const fakePost = new Posst();
            const fakeLikedPost = { ...fakePost, likedBy: Promise.resolve([fakeUser]) };

            const fakeFindPostById = jest.spyOn(postService, 'findPostById')
                .mockImplementation(() => Promise.resolve(fakePost));
            const fakePrepareForVoting = jest.spyOn(likesService, 'prepareForVoting')
                .mockImplementation(() => Promise.resolve(true));
            const fakeUpvotePost = jest.spyOn(likesService, 'upvotePost')
                .mockImplementation(() => Promise.resolve(fakeLikedPost));
            const fakeTransformPost = jest.spyOn(postService, 'transformPostForDisplay')
                .mockImplementation(() => Promise.resolve(['result']));

            const result = await controller.vote(fakeId, fakeRequest);

            expect(result).toEqual(['result']);

            fakeFindPostById.mockRestore();
            fakePrepareForVoting.mockRestore();
            fakeUpvotePost.mockRestore();
            fakeTransformPost.mockRestore();

        });
    });
    describe('downvote method', () => {
        it('should call the postService.findPostById method once with correct id', async () => {
            const fakeUser = new User();
            const fakeRequest = {
                user: fakeUser,
            };
            const fakeId: string = '3';

            const fakePost = new Posst();
            const fakeDislikedPost = { ...fakePost, dislikedBy: Promise.resolve([fakeUser]) };

            const fakeFindPostById = jest.spyOn(postService, 'findPostById')
                .mockImplementation(() => Promise.resolve(fakePost));
            const fakePrepareForVoting = jest.spyOn(likesService, 'prepareForVoting')
                .mockImplementation(() => Promise.resolve(true));
            const fakeDownvotePost = jest.spyOn(likesService, 'downvotePost')
                .mockImplementation(() => Promise.resolve(fakeDislikedPost));
            const fakeTransformPost = jest.spyOn(postService, 'transformPostForDisplay')
                .mockImplementation(() => Promise.resolve(['result']));

            await controller.downvote(fakeId, fakeRequest);

            expect(fakeFindPostById).toBeCalledTimes(1);
            expect(fakeFindPostById).toHaveBeenCalledWith(fakeId);

            fakeFindPostById.mockRestore();
            fakePrepareForVoting.mockRestore();
            fakeDownvotePost.mockRestore();
            fakeTransformPost.mockRestore();

        });
        it('should call the likesService.prepareForVoting method once when post is found', async () => {
            const fakeUser = new User();
            const fakeRequest = {
                user: fakeUser,
            };
            const fakeId: string = '3';

            const fakePost = new Posst();
            const fakeDislikedPost = { ...fakePost, dislikedBy: Promise.resolve([fakeUser]) };
            const fakeFindPostById = jest.spyOn(postService, 'findPostById')
                .mockImplementation(() => Promise.resolve(fakePost));
            const fakePrepareForVoting = jest.spyOn(likesService, 'prepareForVoting')
                .mockImplementation(() => Promise.resolve(true));
            const fakeDownvotePost = jest.spyOn(likesService, 'downvotePost')
                .mockImplementation(() => Promise.resolve(fakeDislikedPost));
            const fakeTransformPost = jest.spyOn(postService, 'transformPostForDisplay')
                .mockImplementation(() => Promise.resolve(['result']));

            await controller.downvote(fakeId, fakeRequest);

            expect(fakePrepareForVoting).toBeCalledTimes(1);

            fakeFindPostById.mockRestore();
            fakePrepareForVoting.mockRestore();
            fakeDownvotePost.mockRestore();
            fakeTransformPost.mockRestore();

        });
        it('should not call the likesService.prepareForVoting method if post was not found by findPostById method', async () => {
            const fakeUser = new User();
            const fakeRequest = {
                user: fakeUser,
            };
            const fakeId: string = '3';

            const fakePost = new Posst();
            const fakeDislikedPost = { ...fakePost, dislikedBy: Promise.resolve([fakeUser]) };
            const fakeFindPostById = jest.spyOn(postService, 'findPostById')
                .mockImplementation(() => { throw new PostNotFound() });
            const fakePrepareForVoting = jest.spyOn(likesService, 'prepareForVoting')
                .mockImplementation(() => Promise.resolve(true));
            const fakeDownvotePost = jest.spyOn(likesService, 'downvotePost')
                .mockImplementation(() => Promise.resolve(fakeDislikedPost));
            const fakeTransformPost = jest.spyOn(postService, 'transformPostForDisplay')
                .mockImplementation(() => Promise.resolve(['result']));
            try {
                await controller.downvote(fakeId, fakeRequest);
            } catch (err) {}

            expect(fakePrepareForVoting).not.toHaveBeenCalled();

            fakeFindPostById.mockRestore();
            fakePrepareForVoting.mockRestore();
            fakeDownvotePost.mockRestore();
            fakeTransformPost.mockRestore();
        });
        it('should call the likesService.downvotePost method once with correct parameters when post is found', async () => {
            const fakeUser = new User();
            const fakeRequest = {
                user: fakeUser,
            };
            const fakeId: string = '3';

            const fakePost = new Posst();
            const fakeDislikedPost = { ...fakePost, dislikedBy: Promise.resolve([fakeUser]) };
            const fakeFindPostById = jest.spyOn(postService, 'findPostById')
                .mockImplementation(() => Promise.resolve(fakePost));
            const fakePrepareForVoting = jest.spyOn(likesService, 'prepareForVoting')
                .mockImplementation(() => Promise.resolve(true));
            const fakeDownvotePost = jest.spyOn(likesService, 'downvotePost')
                .mockImplementation(() => Promise.resolve(fakeDislikedPost));
            const fakeTransformPost = jest.spyOn(postService, 'transformPostForDisplay')
                .mockImplementation(() => Promise.resolve(['result']));

            await controller.downvote(fakeId, fakeRequest);

            expect(fakeDownvotePost).toBeCalledTimes(1);
            expect(fakeDownvotePost).toHaveBeenCalledWith(fakePost, fakeUser);

            fakeFindPostById.mockRestore();
            fakePrepareForVoting.mockRestore();
            fakeDownvotePost.mockRestore();
            fakeTransformPost.mockRestore();

        });
        it('should not call the likesService.downvotePost method if likesService.prepareForVoting method throws', async () => {
            const fakeUser = new User();
            const fakeRequest = {
                user: fakeUser,
            };
            const fakeId: string = '3';

            const fakePost = new Posst();
            const fakeDislikedPost = { ...fakePost, dislikedBy: Promise.resolve([fakeUser]) };
            const fakeFindPostById = jest.spyOn(postService, 'findPostById')
                .mockImplementation(() => Promise.resolve(fakePost));
            const fakePrepareForVoting = jest.spyOn(likesService, 'prepareForVoting')
                .mockImplementation(() => { throw new BlockedAccessException() });
            const fakeDownvotePost = jest.spyOn(likesService, 'downvotePost')
                .mockImplementation(() => Promise.resolve(fakeDislikedPost));
            const fakeTransformPost = jest.spyOn(postService, 'transformPostForDisplay')
                .mockImplementation(() => Promise.resolve(['result']));

            try {
                await controller.downvote(fakeId, fakeRequest);
            } catch (err) { }

            expect(fakeDownvotePost).not.toHaveBeenCalled();

            fakeFindPostById.mockRestore();
            fakePrepareForVoting.mockRestore();
            fakeDownvotePost.mockRestore();
            fakeTransformPost.mockRestore();

        });
        it('should call the postService.transformPostForDisplay method if post is found and prepareForVoting method does not throw', async () => {
            const fakeUser = new User();
            const fakeRequest = {
                user: fakeUser,
            };
            const fakeId: string = '3';
            const fakePost = new Posst();
            const fakeDislikedPost = { ...fakePost, dislikedBy: Promise.resolve([fakeUser]) };

            const fakeFindPostById = jest.spyOn(postService, 'findPostById')
                .mockImplementation(() => Promise.resolve(fakePost));
            const fakePrepareForVoting = jest.spyOn(likesService, 'prepareForVoting')
                .mockImplementation(() => Promise.resolve(true));
            const fakeDownvotePost = jest.spyOn(likesService, 'downvotePost')
                .mockImplementation(() => Promise.resolve(fakeDislikedPost));
            const fakeTransformPost = jest.spyOn(postService, 'transformPostForDisplay')
                .mockImplementation(() => Promise.resolve(['result']));

            await controller.downvote(fakeId, fakeRequest);

            expect(fakeTransformPost).toBeCalledTimes(1);
            expect(fakeTransformPost).toBeCalledWith(fakeDislikedPost);

            fakeFindPostById.mockRestore();
            fakePrepareForVoting.mockRestore();
            fakeDownvotePost.mockRestore();
            fakeTransformPost.mockRestore();

        });
        it('should return correct result when post is found and voting is allowed by the likesService.prepareForVoting method', async () => {
            const fakeUser = new User();
            const fakeRequest = {
                user: fakeUser,
            };
            const fakeId: string = '3';
            const fakePost = new Posst();
            const fakeDislikedPost = { ...fakePost, dislikedBy: Promise.resolve([fakeUser]) };

            const fakeFindPostById = jest.spyOn(postService, 'findPostById')
                .mockImplementation(() => Promise.resolve(fakePost));
            const fakePrepareForVoting = jest.spyOn(likesService, 'prepareForVoting')
                .mockImplementation(() => Promise.resolve(true));
            const fakeDownvotePost = jest.spyOn(likesService, 'downvotePost')
                .mockImplementation(() => Promise.resolve(fakeDislikedPost));
            const fakeTransformPost = jest.spyOn(postService, 'transformPostForDisplay')
                .mockImplementation(() => Promise.resolve(['result']));

            const result = await controller.downvote(fakeId, fakeRequest);

            expect(result).toEqual(['result']);

            fakeFindPostById.mockRestore();
            fakePrepareForVoting.mockRestore();
            fakeDownvotePost.mockRestore();
            fakeTransformPost.mockRestore();
        });
    });
    describe('flag method', () => {
        it('flag should call findPostById method once with correct id', async () => {
            const fakeId: string = '3';

            const fakePost = new Posst();
            const fakeFindPostById = jest.spyOn(postService, 'findPostById')
                .mockImplementation(() => Promise.resolve(fakePost));
            const fakeFlag = jest.spyOn(postService, 'flagPostById')
                .mockImplementation(() => Promise.resolve(['flagged']));

            await controller.flag(fakeId);

            expect(fakeFindPostById).toBeCalledTimes(1);
            expect(fakeFindPostById).toHaveBeenCalledWith(fakeId);

            fakeFindPostById.mockRestore();
            fakeFlag.mockRestore();
        });
        it('flag should call the flagPostById method once with correct post', async () => {
            const fakeId: string = '3';

            const fakePost = new Posst();
            const fakeFindPostById = jest.spyOn(postService, 'findPostById')
                .mockImplementation(() => Promise.resolve(fakePost));
            const fakeFlag = jest.spyOn(postService, 'flagPostById')
                .mockImplementation(() => Promise.resolve(['flagged']));

            await controller.flag(fakeId);

            expect(fakeFlag).toBeCalledTimes(1);
            expect(fakeFlag).toHaveBeenCalledWith(fakePost);

            fakeFindPostById.mockRestore();
            fakeFlag.mockRestore();
        });

        it('flag should return the correct result from the flagPostById method', async () => {
            const fakeId: string = '3';

            const fakePost = new Posst();
            const fakeFindPostById = jest.spyOn(postService, 'findPostById')
                .mockImplementation(() => Promise.resolve(fakePost));
            const fakeFlag = jest.spyOn(postService, 'flagPostById')
                .mockImplementation(() => Promise.resolve(['flagged']));
            const result = await controller.flag(fakeId);
            expect(result).toEqual(['flagged']);
            fakeFindPostById.mockRestore();
            fakeFlag.mockRestore();
        });
    });
    describe('lock method', () => {
        it('lock should call findPostById method once with correct id', async () => {
            const fakeId: string = '3';

            const fakePost = new Posst();
            const fakeFindPostById = jest.spyOn(postService, 'findPostById')
                .mockImplementation(() => Promise.resolve(fakePost));
            const fakeLock = jest.spyOn(postService, 'lockPostById')
                .mockImplementation(() => Promise.resolve(['locked']));

            await controller.lock(fakeId);

            expect(fakeFindPostById).toBeCalledTimes(1);
            expect(fakeFindPostById).toHaveBeenCalledWith(fakeId);

            fakeFindPostById.mockRestore();
            fakeLock.mockRestore();
        });
        it('lock should call the lockPostById method once with correct post', async () => {
            const fakeId: string = '3';

            const fakePost = new Posst();
            const fakeFindPostById = jest.spyOn(postService, 'findPostById')
                .mockImplementation(() => Promise.resolve(fakePost));
            const fakeLock = jest.spyOn(postService, 'lockPostById')
                .mockImplementation(() => Promise.resolve(['locked']));

            await controller.lock(fakeId);

            expect(fakeLock).toBeCalledTimes(1);
            expect(fakeLock).toHaveBeenCalledWith(fakePost);

            fakeFindPostById.mockRestore();
            fakeLock.mockRestore();
        });

        it('lock should return the correct result from the lockPostById method', async () => {
            const fakeId: string = '3';

            const fakePost = new Posst();
            const fakeFindPostById = jest.spyOn(postService, 'findPostById')
                .mockImplementation(() => Promise.resolve(fakePost));
            const fakeLock = jest.spyOn(postService, 'lockPostById')
                .mockImplementation(() => Promise.resolve(['locked']));

            const result = await controller.lock(fakeId);

            expect(result).toEqual(['locked']);

            fakeFindPostById.mockRestore();
            fakeLock.mockRestore();
        });
    });
});
