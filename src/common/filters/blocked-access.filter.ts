import { ExceptionFilter, Catch, ArgumentsHost } from '@nestjs/common';
import { Response } from 'express';
import { PostSystemException } from './post-system-exception';

@Catch(PostSystemException)
export class BlockedAccessFilter implements ExceptionFilter {
    catch(exception: PostSystemException, host: ArgumentsHost) {
        const ctx = host.switchToHttp();
        const response = ctx.getResponse<Response>();

        response
            .status(403)
            .json({
                statusCode: 403,
                error: exception.message,
            });
    }
}