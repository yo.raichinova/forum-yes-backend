export class PostSystemException extends Error { }

export class PostNotFound extends PostSystemException {}
export class BlockedAccessException extends PostSystemException {}
export class UserNotFound extends PostSystemException {}
export class BadRequest extends PostSystemException {}
export class CommentNotFound extends PostSystemException {}