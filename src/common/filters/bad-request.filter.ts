import { ExceptionFilter, Catch, ArgumentsHost } from '@nestjs/common';
import { Response } from 'express';
import { PostSystemException } from './post-system-exception';

@Catch(PostSystemException)
export class BadRequestFilter implements ExceptionFilter {
    catch(exception: PostSystemException, host: ArgumentsHost) {
        const ctx = host.switchToHttp();
        const response = ctx.getResponse<Response>();

        response
            .status(400)
            .json({
                statusCode: 400,
                error: exception.message,
            });
    }
}