
import { ExceptionFilter, Catch, ArgumentsHost } from '@nestjs/common';
import { Response } from 'express';
import { PostSystemException } from './post-system-exception';

@Catch(PostSystemException)
export class NotFoundFilter implements ExceptionFilter {
    catch(exception: PostSystemException, host: ArgumentsHost) {
        const ctx = host.switchToHttp();
        const response = ctx.getResponse<Response>();

        response
            .status(404)
            .json({
                statusCode: 404,
                error: exception.message,
            });
    }
}