import { Controller, Get, Param, HttpStatus, Delete, Put, Body, ValidationPipe, UseGuards, Post, Req, UseFilters, HttpCode, Query } from '@nestjs/common';
import { UserService } from '../core/services/user.service';
import { StatusService } from '../core/services/status.service';
import { StatusDescribeDTO } from '../models/status-describe-dto';
import { Roles } from '../decorators/roles.decorator';
import { RolesGuard } from '../guards/roles.guard';
import { AuthGuard } from '@nestjs/passport';
import { UserReturnDTO } from '../models/user-return-dto';
import { plainToClass } from 'class-transformer';
import { StatusGuard } from '../guards/status.guard';
import { NotFoundFilter } from '../common/filters/not-found.filter';
import { BlockedAccessFilter } from '../common/filters/blocked-access.filter';
import { PostDisplayDTO } from '../models/post-display-dto';
import { TransformService } from '../core/services/transform.service';
import { User } from 'src/entities/user.entity';
import { UserQueryDTO } from '../models/user-query-dto';

@Controller('users')
@UseGuards(AuthGuard('jwt'))

export class UserController {
    constructor(
        private readonly userService: UserService,
        private readonly statusService: StatusService,
        private readonly transformService: TransformService,
    ) { }

    @Get()
    @HttpCode(HttpStatus.OK)
    async all(@Query() query: UserQueryDTO): Promise<UserReturnDTO[]> {
        const userArr = await this.userService.findAll(query);
        return await this.transformService.transformUserArray(userArr);
    }

    @Get(':userId')
    @HttpCode(HttpStatus.OK)
    @UseFilters(NotFoundFilter)
    async getUserById(@Param('userId') id: string): Promise<UserReturnDTO> {
        const userFound = await this.userService.findUserById(id);
        const userTransformed = await this.transformService.transformUser(userFound);
        return userTransformed;
    }

    @Post(':userId/friends')
    @HttpCode(HttpStatus.CREATED)
    @UseFilters(NotFoundFilter)
    @UseGuards(StatusGuard)
    async addFriend(
        @Param('userId') id: string,
        @Req() request,
    ): Promise<User> {
        const userToBeStalked = await this.userService.findUserById(id);
        const userToBeStalkedId = await userToBeStalked.id;
        const stalkingUser = request.user;
        return this.userService.addFriend(stalkingUser, userToBeStalkedId);
    }

    @Put(':userId/banstatus')
    @HttpCode(HttpStatus.OK)
    @UseFilters(NotFoundFilter, BlockedAccessFilter)
    @Roles('admin')
    @UseGuards(RolesGuard)
    async changeBanStatus(
        @Param('userId') id: string,
        @Body(new ValidationPipe({ whitelist: true, transform: true })) description: StatusDescribeDTO,
    ): Promise<{ message: string }> {
        const userFound = await this.userService.findUserById(id);

        const bannedUser = await this.statusService.updateStatus(description, userFound);

        await this.transformService.transformUser(bannedUser);
        return { message: "User successfully banned" };
    }

    @Put(':userId/friends')
    @HttpCode(HttpStatus.OK)
    @UseFilters(NotFoundFilter)
    @UseGuards(StatusGuard)
    async removeFriend(
        @Param('userId') id: string,
        @Req() request,
    ): Promise<User> {
        const userToStopBeingStalked = await this.userService.findUserById(id);
        const userToStopBeingStalkedId = await userToStopBeingStalked.id;
        const stalkingUser = request.user;
        return await this.userService.removeFriend(userToStopBeingStalkedId, stalkingUser);
    }

    @Delete(':userId')
    @HttpCode(HttpStatus.OK)
    @UseFilters(NotFoundFilter, BlockedAccessFilter)
    @Roles('admin')
    @UseGuards(RolesGuard)
    async delete(@Param('userId') id: string): Promise<UserReturnDTO> {
        const userForDeletion = await this.userService.findUserById(id);
        const deletedUser = await this.userService.deleteUser(userForDeletion);
        return plainToClass(UserReturnDTO, deletedUser, { excludeExtraneousValues: true });
    }
}
