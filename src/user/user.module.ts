import { Module } from '@nestjs/common';
import { UserController } from './user.controller';
import { User } from '../entities/user.entity';
import { CoreModule } from '../core/core.module';
import { AuthModule } from '../auth/auth.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Status } from '../entities/status.entity';
import { Posst } from '../entities/post.entity';
import { StatusService } from '../core/services/status.service';
import { PostService } from '../post/post.service';
import { UserService } from '../core/services/user.service';

@Module({
  imports: [TypeOrmModule.forFeature([User, Status, Posst]), CoreModule, AuthModule],
  controllers: [UserController],
  providers: [StatusService, PostService, UserService],
})
export class UserModule {}
