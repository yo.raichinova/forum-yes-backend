
import { Entity, Column, PrimaryGeneratedColumn, OneToMany, JoinColumn, OneToOne, ManyToMany, JoinTable } from 'typeorm';
import { Posst } from './post.entity';
import { Comment } from './comment.entity';
import { Status } from './status.entity';

export enum UserRole {
    ADMIN = 'admin',
    MEMBER = 'member',
}

@Entity()
export class User {
    @PrimaryGeneratedColumn()
    id: number;

    @Column('nvarchar', { length: 50 })
    name: string;

    @Column('nvarchar', { length: 200 })
    password: string;

    @Column('nvarchar', { length: 50 })
    email: string;

    @Column({
        type: 'enum',
        enum: UserRole,
        default: UserRole.MEMBER,
    })
    role: UserRole;

    @OneToMany(type => Posst, post => post.user)
    posts: Promise<Posst[]>;

    @OneToMany(type => Comment, comment => comment.user)
    comments: Promise<Comment[]>;

    @JoinTable()
    @ManyToMany(type => User, user => user.stalkedBy)
    stalking: Promise<User[]>;

    @ManyToMany(type => User, user => user.stalking)
    stalkedBy: Promise<User[]>;

    @Column({
        default: false,
    })
    isDeleted: boolean;

    @OneToOne(type => Status, status => status.user)
    @JoinColumn()
    status: Promise<Status>;

    @ManyToMany(type => Posst, post => post.likedBy)
    postLikes: Promise<Posst[]>;

    @ManyToMany(type => Posst, post => post.dislikedBy)
    postDislikes: Promise<Posst[]>;

    @ManyToMany(type => Comment, comment => comment.likedBy)
    commentLikes: Promise<Comment[]>;

    @ManyToMany(type => Comment, comment => comment.dislikedBy)
    commentDislikes: Promise<Comment[]>;
}

