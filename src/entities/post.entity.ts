
import {
    Entity,
    Column,
    PrimaryGeneratedColumn,
    ManyToOne,
    OneToMany,
    CreateDateColumn,
    UpdateDateColumn,
    ManyToMany,
    JoinTable,
    Index,
} from 'typeorm';
import { Comment } from './comment.entity';
import { User } from './user.entity';

@Entity()
export class Posst {
    @PrimaryGeneratedColumn()
    id: number;

    @Index({ fulltext: true })
    @Column('nvarchar', { length: 200 })
    title: string;

    @Index({ fulltext: true })
    @Column('nvarchar', { length: 2500 })
    content: string;

    @ManyToOne(type => User, user => user.posts)
    user: Promise<User>;

    @OneToMany(type => Comment, comment => comment.post)
    comments: Promise<Comment[]>;

    @CreateDateColumn({ type: 'timestamp' })
    createdAt: Date;

    @UpdateDateColumn({ type: 'timestamp' })
    updatedAt?: Date;

    @Column({
        default: false,
    })
    isDeleted: boolean;

    @Column({
        default: false,
    })
    isFlagged: boolean;

    @Column({
        default: false,
    })
    isLocked: boolean;

    @JoinTable()
    @ManyToMany(type => User, user => user.postLikes)
    likedBy: Promise<User[]>;

    @JoinTable()
    @ManyToMany(type => User, user => user.postDislikes)
    dislikedBy: Promise<User[]>;
}
