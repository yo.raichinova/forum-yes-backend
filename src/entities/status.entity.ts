import { Entity, Column, PrimaryGeneratedColumn, OneToOne } from 'typeorm';
import { User } from './user.entity';

@Entity()
export class Status {

    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        default: false,
    })
    isBanned: boolean;

    @Column('nvarchar', {
        default: 'this user has not misbehaved',
        length: 500 })
    description: string;

    @OneToOne(type => User, user => user.status)
    user: User;
}
