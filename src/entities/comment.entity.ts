
import { Entity, Column, PrimaryGeneratedColumn, ManyToOne, CreateDateColumn, UpdateDateColumn, JoinColumn, JoinTable, ManyToMany } from 'typeorm';
import { Posst } from './post.entity';
import { User } from './user.entity';

@Entity()
export class Comment {
    @PrimaryGeneratedColumn()
    id: number;

    @Column('text')
    content: string;

    @ManyToOne(type => User, user => user.comments)
    user: Promise<User>;

    @ManyToOne(type => Posst, post => post.comments)
    post: Promise<Posst>;

    @CreateDateColumn({ type: 'timestamp' })
    createdAt: Date;

    @UpdateDateColumn({ type: 'timestamp' })
    updatedAt: Date;

    @Column({
        default: false,
    })
    isDeleted: boolean;

    @JoinTable()
    @ManyToMany(type => User, user => user.commentLikes)
    likedBy: Promise<User[]>;

    @JoinTable()
    @ManyToMany(type => User, user => user.commentDislikes)
    dislikedBy: Promise<User[]>;

    @Column({
        default: false,
    })
    isFlagged: boolean;

    @Column({
        default: false,
    })
    isLocked: boolean;
}
