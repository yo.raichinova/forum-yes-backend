import { Injectable, CanActivate, ExecutionContext } from '@nestjs/common';
import { PostService } from '../post/post.service';


@Injectable()
export class LockGuard implements CanActivate {
   constructor(private readonly postService: PostService) { }

   async canActivate(context: ExecutionContext): Promise<boolean> {
       const request = context.switchToHttp().getRequest();
      // const user = request.user.role;

       const post = await this.postService.findPostById(request.params.postId);

       const locked = post.isLocked;

       return !locked;
   }
}
