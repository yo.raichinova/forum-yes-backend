import { CommentController } from './comment.controller';
import { TestingModule, Test } from '@nestjs/testing';
import { CommentService } from './comment.service';
import { PostService } from '../post/post.service';
import { LikesService } from '../core/services/likes.service';
import { Posst } from '../entities/post.entity';
import { User } from '../entities/user.entity';
import { Comment } from '../entities/comment.entity';

describe('CommentController', () => {
    let controller: CommentController;

    const commentService = {
        findAll() { },
        add() { },
        findCommentById() { },
        checkAuthor() { },
        updateCommentById() { },
        transformCommentForDisplay() { },
        deleteCommentById() { },

    };

    const postService = {
        findPostById() { },
    };

    const likesService = {
        prepareForVoting() { },

        upvoteComment() { },

        downvoteComment() { }
    };

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            controllers: [CommentController],
            providers: [
                {
                    provide: CommentService,
                    useValue: commentService,
                },
                {
                    provide: PostService,
                    useValue: postService,
                },
                {
                    provide: LikesService,
                    useValue: likesService,
                },
            ],
        }).compile();

        controller = module.get<CommentController>(CommentController);
    });

    it('should be defined', () => {
        expect(controller).toBeDefined();
    });
    describe('all', () => {
        it('should return the correct result from findPostById method', async () => {
            const fakeId = '7';
            const fakePost = new Posst();

            const fakeFindPostById = jest.spyOn(postService, 'findPostById')
                .mockImplementation(() => Promise.resolve(fakePost));
            const fakeShowComments = jest.spyOn(commentService, 'findAll')
                .mockImplementation(() => Promise.resolve(['comment']));

            const result = await controller.all(fakeId);

            expect(result).toEqual(['comment']);

            fakeFindPostById.mockRestore();
            fakeShowComments.mockRestore();
        });
        it('should call findPostById method with correct id', async () => {
            const fakeId = '77';
            const fakePost = new Posst();

            const fakeFindPostById = jest.spyOn(postService, 'findPostById')
                .mockImplementation(() => Promise.resolve(fakePost));
            const fakeShowComments = jest.spyOn(commentService, 'findAll')
                .mockImplementation(() => Promise.resolve(['comment']));

            await controller.all(fakeId);

            expect(fakeFindPostById).toHaveBeenCalled();
            expect(fakeFindPostById).toHaveBeenCalledWith(fakeId);

            fakeFindPostById.mockRestore();
            fakeShowComments.mockRestore();
        });
        it('should call findAll method with correct parameter', async () => {
            const fakeId = '77';
            const fakePost = new Posst();

            const fakeFindPostById = jest.spyOn(postService, 'findPostById')
                .mockImplementation(() => Promise.resolve(fakePost));
            const fakeShowComments = jest.spyOn(commentService, 'findAll')
                .mockImplementation(() => Promise.resolve(['comment']));

            await controller.all(fakeId);

            expect(fakeShowComments).toHaveBeenCalled();
            expect(fakeShowComments).toHaveBeenCalledWith(fakePost);

            fakeFindPostById.mockRestore();
            fakeShowComments.mockRestore();
        });
    });

    describe('add', () => {
        it('should return correct result from the commentService.add method', async () => {
            const fakeId = '77';
            const fakeUser = new User();
            const fakeRequest = {
                user: fakeUser,
            };
            const fakeCommentDto = {
                content: 'some content',
            };

            const fakePostFound = jest.spyOn(postService, 'findPostById')
                .mockImplementation(() => Promise.resolve(fakeId));
            const fakeCommentAdd = jest.spyOn(commentService, 'add')
                .mockImplementation(() => Promise.resolve(fakeCommentDto));

            const result = await controller.add(fakeId, fakeCommentDto, fakeRequest);

            expect(result).toEqual(fakeCommentDto);

            fakePostFound.mockRestore();
            fakeCommentAdd.mockRestore();
        });
        it('should call postService.findPostById method with correct id', async () => {
            const fakeId = '77';
            const fakeUser = new User();
            const fakeRequest = {
                user: fakeUser,
            };
            const fakeCommentDto = {
                content: 'some content',
            };

            const fakeFindPostById = jest.spyOn(postService, 'findPostById')
                .mockImplementation(() => Promise.resolve(fakeFindPostById));
            const fakeCommentAdd = jest.spyOn(commentService, 'add')
                .mockImplementation(() => Promise.resolve(fakeCommentDto));

            await controller.add(fakeId, fakeCommentDto, fakeRequest);

            expect(fakeFindPostById).toHaveBeenCalled();
            expect(fakeFindPostById).toHaveBeenCalledWith(fakeId);

            fakeFindPostById.mockRestore();
            fakeCommentAdd.mockRestore();
        });
        it('should call commentService.add method with correct comment', async () => {
            const fakeId = '77';
            const fakeUser = new User();
            const fakeRequest = {
                user: fakeUser,
            };
            const fakeCommentDto = {
                content: 'some content',
            };

            const fakeFindPostById = jest.spyOn(postService, 'findPostById')
                .mockImplementation(() => Promise.resolve(fakeFindPostById));
            const fakeCommentAdd = jest.spyOn(commentService, 'add')
                .mockImplementation(() => Promise.resolve(fakeCommentDto));

            await controller.add(fakeId, fakeCommentDto, fakeRequest);

            expect(fakeCommentAdd).toHaveBeenCalled();
            expect(fakeCommentAdd).toHaveBeenCalledWith(fakeCommentDto, fakeRequest.user, fakeFindPostById);

            fakeFindPostById.mockRestore();
            fakeCommentAdd.mockRestore();
        });

    });

    describe('update', () => {
        it('should return correct result', async () => {
            const fakeId = '77';
            const fakeUser = new User();
            const fakePost = new Posst();
            const fakeCommentData = {
                content: 'some great content',
            };
            const fakeRequest = {
                user: fakeUser,
            };

            const fakeFindCommentById = jest.spyOn(commentService, 'findCommentById')
                .mockImplementation(() => Promise.resolve(fakePost));
            const fakeCheckAuthor = jest.spyOn(commentService, 'checkAuthor')
                .mockImplementation(() => Promise.resolve(true));
            const fakeUpdateCommentById = jest.spyOn(commentService, 'updateCommentById')
                .mockImplementation(() => Promise.resolve('comment'));

            const result = await controller.update(fakeId, fakeCommentData, fakeRequest);

            expect(result).toEqual('comment');

            fakeFindCommentById.mockRestore();
            fakeCheckAuthor.mockRestore();
            fakeUpdateCommentById.mockRestore();
        });
        it('should call the commentService.findCommentById method once with correct id', async () => {
            const fakeId = '77';
            const fakeUser = new User();
            const fakePost = new Posst();
            const fakeCommentData = {
                content: 'some great content',
            };
            const fakeRequest = {
                user: fakeUser,
            };

            const fakeFindCommentById = jest.spyOn(commentService, 'findCommentById')
                .mockImplementation(() => Promise.resolve(fakePost));
            const fakeCheckAuthor = jest.spyOn(commentService, 'checkAuthor')
                .mockImplementation(() => Promise.resolve(true));
            const fakeUpdateCommentById = jest.spyOn(commentService, 'updateCommentById')
                .mockImplementation(() => Promise.resolve('comment'));

            await controller.update(fakeId, fakeCommentData, fakeRequest);

            expect(fakeFindCommentById).toHaveBeenCalled();
            expect(fakeFindCommentById).toHaveBeenCalledWith(fakeId);

            fakeFindCommentById.mockRestore();
            fakeCheckAuthor.mockRestore();
            fakeUpdateCommentById.mockRestore();
        });
        it('should call the commentService.checkAuthor method with correct parameters', async () => {
            const fakeId = '77';
            const fakeUser = new User();
            const fakePost = new Posst();
            const fakeCommentData = {
                content: 'some great content',
            };
            const fakeRequest = {
                user: fakeUser,
            };

            const fakeFindCommentById = jest.spyOn(commentService, 'findCommentById')
                .mockImplementation(() => Promise.resolve(fakePost));
            const fakeCheckAuthor = jest.spyOn(commentService, 'checkAuthor')
                .mockImplementation(() => Promise.resolve(true));
            const fakeUpdateCommentById = jest.spyOn(commentService, 'updateCommentById')
                .mockImplementation(() => Promise.resolve('comment'));

            await controller.update(fakeId, fakeCommentData, fakeRequest);

            expect(fakeCheckAuthor).toHaveBeenCalled();
            expect(fakeCheckAuthor).toHaveBeenCalledWith(await fakePost.user, fakeUser);

            fakeFindCommentById.mockRestore();
            fakeCheckAuthor.mockRestore();
            fakeUpdateCommentById.mockRestore();
        });
        it('should call the commentService.updateCommentById method with correct parameters', async () => {
            const fakeId = '77';
            const fakeUser = new User();
            const fakePost = new Posst();
            const fakeCommentData = {
                content: 'some great content',
            };
            const fakeRequest = {
                user: fakeUser,
            };

            const fakeFindCommentById = jest.spyOn(commentService, 'findCommentById')
                .mockImplementation(() => Promise.resolve(fakePost));
            const fakeCheckAuthor = jest.spyOn(commentService, 'checkAuthor')
                .mockImplementation(() => Promise.resolve(true));
            const fakeUpdateCommentById = jest.spyOn(commentService, 'updateCommentById')
                .mockImplementation(() => Promise.resolve('comment'));

            await controller.update(fakeId, fakeCommentData, fakeRequest);

            expect(fakeUpdateCommentById).toHaveBeenCalled();
            expect(fakeUpdateCommentById).toHaveBeenCalledWith(fakeCommentData, fakeId);

            fakeFindCommentById.mockRestore();
            fakeCheckAuthor.mockRestore();
            fakeUpdateCommentById.mockRestore();
        });
    });

    describe('vote', () => {
        it('should return correct result when comment is found and liking is enabled by the likesService.prepareForVoting method', async () => {
            const fakeUser = new User();
            const fakeRequest = {
                user: fakeUser,
            };
            const fakeId = '777';
            const fakeComment = {
                content: 'some awesome content',
            };
            const fakePost = new Posst();
            const fakeLikedComment = { ...fakeComment, likedBy: Promise.resolve([fakeUser]) };

            const fakeFindCommentById = jest.spyOn(commentService, 'findCommentById')
                .mockImplementation(() => Promise.resolve(fakePost));
            const fakePrepareForVoting = jest.spyOn(likesService, 'prepareForVoting')
                .mockImplementation(() => Promise.resolve(true));
            const fakeUpvoteComment = jest.spyOn(likesService, 'upvoteComment')
                .mockImplementation(() => Promise.resolve(fakeLikedComment));
            const fakeTransformComment = jest.spyOn(commentService, 'transformCommentForDisplay')
                .mockImplementation(() => Promise.resolve('comment'));

            const result = await controller.vote(fakeId, fakeRequest);

            expect(result).toEqual('comment');

            fakeFindCommentById.mockRestore();
            fakePrepareForVoting.mockRestore();
            fakeUpvoteComment.mockRestore();
            fakeTransformComment.mockRestore();
        });
        it('should call the commentService.findCommentById method with correct id', async () => {
            const fakeUser = new User();
            const fakeRequest = {
                user: fakeUser,
            };
            const fakeId = '777';
            const fakeComment = {
                content: 'some awesome content',
            };
            const fakePost = new Posst();
            const fakeLikedComment = { ...fakeComment, likedBy: Promise.resolve([fakeUser]) };

            const fakeFindCommentById = jest.spyOn(commentService, 'findCommentById')
                .mockImplementation(() => Promise.resolve(fakePost));
            const fakePrepareForVoting = jest.spyOn(likesService, 'prepareForVoting')
                .mockImplementation(() => Promise.resolve(true));
            const fakeUpvoteComment = jest.spyOn(likesService, 'upvoteComment')
                .mockImplementation(() => Promise.resolve(fakeLikedComment));
            const fakeTransformComment = jest.spyOn(commentService, 'transformCommentForDisplay')
                .mockImplementation(() => Promise.resolve('comment'));

            await controller.vote(fakeId, fakeRequest);

            expect(fakeFindCommentById).toHaveBeenCalled();
            expect(fakeFindCommentById).toHaveBeenCalledWith(fakeId);

            fakeFindCommentById.mockRestore();
            fakePrepareForVoting.mockRestore();
            fakeUpvoteComment.mockRestore();
            fakeTransformComment.mockRestore();
        });
        it('should call the likesService.prepareForVoting method when comment is found', async () => {
            const fakeUser = new User();
            const fakeRequest = {
                user: fakeUser,
            };
            const fakeId = '777';
            const fakeComment = {
                content: 'some awesome content',
            };
            const fakePost = new Posst();
            const fakeLikedComment = { ...fakeComment, likedBy: Promise.resolve([fakeUser]) };

            const fakeFindCommentById = jest.spyOn(commentService, 'findCommentById')
                .mockImplementation(() => Promise.resolve(fakePost));
            const fakePrepareForVoting = jest.spyOn(likesService, 'prepareForVoting')
                .mockImplementation(() => Promise.resolve(true));
            const fakeUpvoteComment = jest.spyOn(likesService, 'upvoteComment')
                .mockImplementation(() => Promise.resolve(fakeLikedComment));
            const fakeTransformComment = jest.spyOn(commentService, 'transformCommentForDisplay')
                .mockImplementation(() => Promise.resolve('comment'));

            await controller.vote(fakeId, fakeRequest);

            expect(fakePrepareForVoting).toHaveBeenCalled();

            fakeFindCommentById.mockRestore();
            fakePrepareForVoting.mockRestore();
            fakeUpvoteComment.mockRestore();
            fakeTransformComment.mockRestore();
        });
        it('should call the likesService.upvoteComment method with correct parameters when comment is found', async () => {
            const fakeUser = new User();
            const fakeRequest = {
                user: fakeUser,
            };
            const fakeId = '777';
            const fakeComment = new Comment();
            const fakePost = new Posst();
            const fakeLikedComment = { ...fakeComment, likedBy: Promise.resolve([fakeUser]) };

            const fakeFindCommentById = jest.spyOn(commentService, 'findCommentById')
                .mockImplementation(() => Promise.resolve(fakePost));
            const fakePrepareForVoting = jest.spyOn(likesService, 'prepareForVoting')
                .mockImplementation(() => Promise.resolve(true));
            const fakeUpvoteComment = jest.spyOn(likesService, 'upvoteComment')
                .mockImplementation(() => Promise.resolve(fakeLikedComment));
            const fakeTransformComment = jest.spyOn(commentService, 'transformCommentForDisplay')
                .mockImplementation(() => Promise.resolve('comment'));

            await controller.vote(fakeId, fakeRequest);

            expect(fakeUpvoteComment).toHaveBeenCalled();
            expect(fakeUpvoteComment).toHaveBeenCalledWith(fakeComment, fakeUser);

            fakeFindCommentById.mockRestore();
            fakePrepareForVoting.mockRestore();
            fakeUpvoteComment.mockRestore();
            fakeTransformComment.mockRestore();
        });
        it('should call the commentService.transformCommentForDisplay method with correct parameters when comment is found', async () => {
            const fakeUser = new User();
            const fakeRequest = {
                user: fakeUser,
            };
            const fakeId = '777';
            const fakeComment = {
                content: 'some awesome content',
            };
            const fakePost = new Posst();
            const fakeLikedComment = { ...fakeComment, likedBy: Promise.resolve([fakeUser]) };

            const fakeFindCommentById = jest.spyOn(commentService, 'findCommentById')
                .mockImplementation(() => Promise.resolve(fakePost));
            const fakePrepareForVoting = jest.spyOn(likesService, 'prepareForVoting')
                .mockImplementation(() => Promise.resolve(true));
            const fakeUpvoteComment = jest.spyOn(likesService, 'upvoteComment')
                .mockImplementation(() => Promise.resolve(fakeLikedComment));
            const fakeTransformComment = jest.spyOn(commentService, 'transformCommentForDisplay')
                .mockImplementation(() => Promise.resolve('comment'));

            await controller.vote(fakeId, fakeRequest);

            expect(fakeTransformComment).toHaveBeenCalled();
            expect(fakeTransformComment).toHaveBeenCalledWith(fakeLikedComment);

            fakeFindCommentById.mockRestore();
            fakePrepareForVoting.mockRestore();
            fakeUpvoteComment.mockRestore();
            fakeTransformComment.mockRestore();
        });
    });

    describe('downvote', () => {
        it('should return correct result when comment is found and disliking is enabled by the likesService.prepareForVoting method', async () => {
            const fakeUser = new User();
            const fakeRequest = {
                user: fakeUser,
            };
            const fakeId = '777';
            const fakeComment = new Comment();
            const fakePost = new Posst();
            const fakeDislikedComment = { ...fakeComment, dislikedBy: Promise.resolve([fakeUser]) };

            const fakeFindCommentById = jest.spyOn(commentService, 'findCommentById')
                .mockImplementation(() => Promise.resolve(fakePost));
            const fakePrepareForVoting = jest.spyOn(likesService, 'prepareForVoting')
                .mockImplementation(() => Promise.resolve(true));
            const fakeDownvoteComment = jest.spyOn(likesService, 'downvoteComment')
                .mockImplementation(() => Promise.resolve(fakeDislikedComment));
            const fakeTransformComment = jest.spyOn(commentService, 'transformCommentForDisplay')
                .mockImplementation(() => Promise.resolve('comment'));

            const result = await controller.downvote(fakeId, fakeRequest);

            expect(result).toEqual('comment');

            fakeFindCommentById.mockRestore();
            fakePrepareForVoting.mockRestore();
            fakeDownvoteComment.mockRestore();
            fakeTransformComment.mockRestore();
        });
    });
    it('should call the commentService.findCommentById method with correct id', async () => {
        const fakeUser = new User();
        const fakeRequest = {
            user: fakeUser,
        };
        const fakeId = '777';
        const fakeComment = {
            content: 'some awesome content',
        };
        const fakePost = new Posst();
        const fakeDislikedComment = { ...fakeComment, dislikedBy: Promise.resolve([fakeUser]) };

        const fakeFindCommentById = jest.spyOn(commentService, 'findCommentById')
            .mockImplementation(() => Promise.resolve(fakePost));
        const fakePrepareForVoting = jest.spyOn(likesService, 'prepareForVoting')
            .mockImplementation(() => Promise.resolve(true));
        const fakeDownvoteComment = jest.spyOn(likesService, 'downvoteComment')
            .mockImplementation(() => Promise.resolve(fakeDislikedComment));
        const fakeTransformComment = jest.spyOn(commentService, 'transformCommentForDisplay')
            .mockImplementation(() => Promise.resolve('comment'));

        await controller.downvote(fakeId, fakeRequest);

        expect(fakeFindCommentById).toHaveBeenCalled();
        expect(fakeFindCommentById).toHaveBeenCalledWith(fakeId);

        fakeFindCommentById.mockRestore();
        fakePrepareForVoting.mockRestore();
        fakeDownvoteComment.mockRestore();
        fakeTransformComment.mockRestore();
    });
    it('should call the likesService.prepareForVoting method when comment is found', async () => {
        const fakeUser = new User();
        const fakeRequest = {
            user: fakeUser,
        };
        const fakeId = '777';
        const fakeComment = {
            content: 'some awesome content',
        };
        const fakePost = new Posst();
        const fakeDislikedComment = { ...fakeComment, dislikedBy: Promise.resolve([fakeUser]) };

        const fakeFindCommentById = jest.spyOn(commentService, 'findCommentById')
            .mockImplementation(() => Promise.resolve(fakePost));
        const fakePrepareForVoting = jest.spyOn(likesService, 'prepareForVoting')
            .mockImplementation(() => Promise.resolve(true));
        const fakeDownvoteComment = jest.spyOn(likesService, 'downvoteComment')
            .mockImplementation(() => Promise.resolve(fakeDislikedComment));
        const fakeTransformComment = jest.spyOn(commentService, 'transformCommentForDisplay')
            .mockImplementation(() => Promise.resolve('comment'));

        await controller.downvote(fakeId, fakeRequest);

        expect(fakePrepareForVoting).toHaveBeenCalled();

        fakeFindCommentById.mockRestore();
        fakePrepareForVoting.mockRestore();
        fakeDownvoteComment.mockRestore();
        fakeTransformComment.mockRestore();
    });
    it('should call the likesService.upvoteComment method with correct parameters when comment is found', async () => {
        const fakeUser = new User();
        const fakeRequest = {
            user: fakeUser,
        };
        const fakeId = '777';
        const fakeComment = new Comment();
        const fakePost = new Posst();
        const fakeDislikedComment = { ...fakeComment, dislikedBy: Promise.resolve([fakeUser]) };

        const fakeFindCommentById = jest.spyOn(commentService, 'findCommentById')
            .mockImplementation(() => Promise.resolve(fakePost));
        const fakePrepareForVoting = jest.spyOn(likesService, 'prepareForVoting')
            .mockImplementation(() => Promise.resolve(true));
        const fakeDownvoteComment = jest.spyOn(likesService, 'downvoteComment')
            .mockImplementation(() => Promise.resolve(fakeDislikedComment));
        const fakeTransformComment = jest.spyOn(commentService, 'transformCommentForDisplay')
            .mockImplementation(() => Promise.resolve('comment'));

        await controller.downvote(fakeId, fakeRequest);

        expect(fakeDownvoteComment).toHaveBeenCalled();
        expect(fakeDownvoteComment).toHaveBeenCalledWith(fakeComment, fakeUser);

        fakeFindCommentById.mockRestore();
        fakePrepareForVoting.mockRestore();
        fakeDownvoteComment.mockRestore();
        fakeTransformComment.mockRestore();
    });
    it('should call the commentService.transformCommentForDisplay method with correct parameters when comment is found', async () => {
        const fakeUser = new User();
        const fakeRequest = {
            user: fakeUser,
        };
        const fakeId = '777';
        const fakeComment = {
            content: 'some awesome content',
        };
        const fakePost = new Posst();
        const fakeDislikedComment = { ...fakeComment, dislikedBy: Promise.resolve([fakeUser]) };

        const fakeFindCommentById = jest.spyOn(commentService, 'findCommentById')
            .mockImplementation(() => Promise.resolve(fakePost));
        const fakePrepareForVoting = jest.spyOn(likesService, 'prepareForVoting')
            .mockImplementation(() => Promise.resolve(true));
        const fakeDownvoteComment = jest.spyOn(likesService, 'downvoteComment')
            .mockImplementation(() => Promise.resolve(fakeDislikedComment));
        const fakeTransformComment = jest.spyOn(commentService, 'transformCommentForDisplay')
            .mockImplementation(() => Promise.resolve('comment'));

        await controller.downvote(fakeId, fakeRequest);

        expect(fakeTransformComment).toHaveBeenCalled();
        expect(fakeTransformComment).toHaveBeenCalledWith(fakeDislikedComment);

        fakeFindCommentById.mockRestore();
        fakePrepareForVoting.mockRestore();
        fakeDownvoteComment.mockRestore();
        fakeTransformComment.mockRestore();
    });

    describe('delete', () => {
        it('should return correct result', async () => {
            const fakeUser = new User();
            const fakeRequest = {
                user: fakeUser,
            };
            const fakeId = '777';

            const fakeFindCommentById = jest.spyOn(commentService, 'findCommentById')
                .mockImplementation(() => Promise.resolve(fakeId));
            const fakeCheckAuthor = jest.spyOn(commentService, 'checkAuthor')
                .mockImplementation(() => Promise.resolve(true));
            const fakeDeleteCommentById = jest.spyOn(commentService, 'deleteCommentById')
                .mockImplementation(() => Promise.resolve('deleted'));

            const result = await controller.delete(fakeId, fakeRequest);

            expect(result).toEqual('deleted');

            fakeFindCommentById.mockRestore();
            fakeCheckAuthor.mockRestore();
            fakeDeleteCommentById.mockRestore();
        });
        it('should call the commentService.findCommentById method once with correct id', async () => {
            const fakeId = '77';
            const fakeUser = new User();
            const fakePost = new Posst();

            const fakeRequest = {
                user: fakeUser,
            };

            const fakeFindCommentById = jest.spyOn(commentService, 'findCommentById')
                .mockImplementation(() => Promise.resolve(fakePost));
            const fakeCheckAuthor = jest.spyOn(commentService, 'checkAuthor')
                .mockImplementation(() => Promise.resolve(true));
            const fakeDeleteCommentById = jest.spyOn(commentService, 'deleteCommentById')
                .mockImplementation(() => Promise.resolve('deleted'));

            await controller.delete(fakeId, fakeRequest);

            expect(fakeFindCommentById).toHaveBeenCalled();
            expect(fakeFindCommentById).toHaveBeenCalledWith(fakeId);

            fakeFindCommentById.mockRestore();
            fakeCheckAuthor.mockRestore();
            fakeDeleteCommentById.mockRestore();
        });
        it('should call the commentService.findCommentById method once with correct id', async () => {
            const fakeId = '77';
            const fakeUser = new User();
            const fakePost = new Posst();
            const fakeRequest = {
                user: fakeUser,
            };

            const fakeFindCommentById = jest.spyOn(commentService, 'findCommentById')
                .mockImplementation(() => Promise.resolve(fakePost));
            const fakeCheckAuthor = jest.spyOn(commentService, 'checkAuthor')
                .mockImplementation(() => Promise.resolve(true));
            const fakeDeleteCommentById = jest.spyOn(commentService, 'deleteCommentById')
                .mockImplementation(() => Promise.resolve('deleted'));

            await controller.delete(fakeId, fakeRequest);

            expect(fakeCheckAuthor).toHaveBeenCalled();
            expect(fakeCheckAuthor).toHaveBeenCalledWith(await fakePost.user, fakeUser);

            fakeFindCommentById.mockRestore();
            fakeCheckAuthor.mockRestore();
            fakeDeleteCommentById.mockRestore();
        });
        it('should call the commentService.findCommentById method once with correct id', async () => {
            const fakeId = '77';
            const fakeUser = new User();
            const fakePost = new Posst();
            const fakeComment = new Comment();
            const fakeRequest = {
                user: fakeUser,
            };

            const fakeFindCommentById = jest.spyOn(commentService, 'findCommentById')
                .mockImplementation(() => Promise.resolve(fakePost));
            const fakeCheckAuthor = jest.spyOn(commentService, 'checkAuthor')
                .mockImplementation(() => Promise.resolve(true));
            const fakeDeleteCommentById = jest.spyOn(commentService, 'deleteCommentById')
                .mockImplementation(() => Promise.resolve('deleted'));

            await controller.delete(fakeId, fakeRequest);

            expect(fakeDeleteCommentById).toHaveBeenCalled();
            expect(fakeDeleteCommentById).toHaveBeenCalledWith(fakeComment);

            fakeFindCommentById.mockRestore();
            fakeCheckAuthor.mockRestore();
            fakeDeleteCommentById.mockRestore();
        });
    });
});
