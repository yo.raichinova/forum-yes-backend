import {
    Controller,
    Get,
    Post,
    Body,
    UseGuards,
    Param,
    ValidationPipe,
    Req,
    HttpStatus,
    Delete,
    Put,
    HttpCode,
    UseFilters,
} from '@nestjs/common';
import { CommentService } from './comment.service';
import { Comment } from '../entities/comment.entity';
import { AuthGuard } from '@nestjs/passport';
import { PostService } from '../post/post.service';
import { CommentCreateDTO } from '../models/comment-create-dto';
import { Posst } from '../entities/post.entity';
import { UpdateResult } from 'typeorm';
import { LikesService } from '../core/services/likes.service';
import { CommentDisplayDTO } from '../models/comment-display-dto';
import { NotFoundFilter } from '../common/filters/not-found.filter';
import { BlockedAccessFilter } from '../common/filters/blocked-access.filter';
import { StatusGuard } from '../guards/status.guard';
import { LockGuard } from '../guards/lock.guard';
import { Roles } from 'src/decorators/roles.decorator';
import { RolesGuard } from 'src/guards/roles.guard';

@Controller('posts/:postId/comments')
@UseGuards(AuthGuard('jwt'))
export class CommentController {

    constructor(private readonly commentService: CommentService,
                private readonly postService: PostService,
                private readonly likesService: LikesService) { }

    @Get()
    @HttpCode(HttpStatus.OK)
    @UseFilters(NotFoundFilter)
    async all(@Param('postId') id: string): Promise<CommentDisplayDTO[]> {
        const postFound = await this.postService.findPostById(id);
        return await this.commentService.findAll(postFound);
    }

    @Post()
    @HttpCode(HttpStatus.CREATED)
    @UseFilters(NotFoundFilter)
    @UseGuards(StatusGuard, LockGuard)
    async add(
        @Param('postId') id: string,
        @Body(new ValidationPipe({ whitelist: true, transform: true })) comment: CommentCreateDTO,
        @Req() request: any): Promise<CommentDisplayDTO> {
        const postFound: Posst = await this.postService.findPostById(id);

        return await this.commentService.add(comment, await request.user, postFound);
    }

    @Put(':commentId')
    @HttpCode(HttpStatus.OK)
    @UseFilters(NotFoundFilter, BlockedAccessFilter)
    @UseGuards(StatusGuard, LockGuard)
    async update(
        @Param('commentId') id: string,
        @Body(new ValidationPipe({ whitelist: true, transform: true })) commentData: CommentCreateDTO,
        @Req() request: any): Promise<{ message: string }> {
        const commentFound = await this.commentService.findCommentById(id);
        const commentAuthor = await commentFound.user;
        await this.commentService.checkAuthor(commentAuthor, request.user);
        return await this.commentService.updateCommentById(commentData, id);

    }

    @Put(':commentId/votes')
    @HttpCode(HttpStatus.OK)
    @UseFilters(NotFoundFilter, BlockedAccessFilter)
    @UseGuards(StatusGuard, LockGuard)
    async vote(@Param('commentId') id: string, @Req() request: any): Promise<CommentDisplayDTO> {
        const commentFound = await this.commentService.findCommentById(id);
        await this.likesService.prepareForVoting(commentFound, request.user, commentFound.likedBy, commentFound.dislikedBy);

        const likedComment = await this.likesService.upvoteComment(commentFound, request.user);
        return this.commentService.transformCommentForDisplay(likedComment);
    }

    @Put(':commentId/downvotes')
    @HttpCode(HttpStatus.OK)
    @UseFilters(NotFoundFilter, BlockedAccessFilter)
    @UseGuards(StatusGuard, LockGuard)
    async downvote(@Param('commentId') id: string, @Req() request: any): Promise<CommentDisplayDTO> {
        const commentFound = await this.commentService.findCommentById(id);
        await this.likesService.prepareForVoting(commentFound, request.user, commentFound.dislikedBy, commentFound.likedBy);

        const dislikedComment = await this.likesService.downvoteComment(commentFound, request.user);
        return this.commentService.transformCommentForDisplay(dislikedComment);
    }

    @Delete(':commentId')
    @HttpCode(HttpStatus.OK)
    @UseFilters(NotFoundFilter, BlockedAccessFilter)
    @UseGuards(StatusGuard, LockGuard)
    async delete(@Param('commentId') id: string, @Req() request: any): Promise<{ message: string }> {
        const commentToBeDeleted: Comment = await this.commentService.findCommentById(id);
        const commentAuthor = await commentToBeDeleted.user;
        await this.commentService.checkAuthor(commentAuthor, request.user);

        return await this.commentService.deleteCommentById(commentToBeDeleted);
    }
    @Put(':commentId/flag')
    @HttpCode(HttpStatus.OK)
    @UseFilters(NotFoundFilter)
    @UseGuards(StatusGuard, LockGuard)
    async flag(@Param('commentId') id: string): Promise<any> {
        const commentForFlagging = await this.commentService.findCommentById(id);
        return await this.commentService.flagCommentById(commentForFlagging);
    }

    @Put(':commentId/lock')
    @HttpCode(HttpStatus.OK)
    @UseFilters(NotFoundFilter)
    @Roles('admin')
    @UseGuards(RolesGuard)
    async lock(@Param('commentId') id: string): Promise<UpdateResult> {
        const commentForLocking = await this.commentService.findCommentById(id);
        return await this.commentService.lockCommentById(commentForLocking);
    }
}
