import { CommentService } from './comment.service';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Comment } from '../entities/comment.entity';
import { TransformService } from '../core/services/transform.service';
import { TestingModule, Test } from '@nestjs/testing';
import { Posst } from '../entities/post.entity';
import { CommentNotFound, BlockedAccessException } from '../common/filters/post-system-exception';
import { User, UserRole } from '../entities/user.entity';

describe('CommentService', () => {
    let service: CommentService;

    const commentRepository = {
        find() { },
        findOne() { },
        save() { },
        update() { }, // Mock implementation of the find method
    };

    const transformService = {
        findAll() { },
        transformCommentForDisplay() { },
        transformComment() { },
        transformCommentArray() { },
        add() { },
    };

    const fakePost = new Posst();

    const fakeComment = new Comment();

    const fakeCommentArr = [fakeComment];

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            providers: [
                CommentService,
                {
                    provide: getRepositoryToken(Comment), // The repository token (instead of the class)
                    useValue: commentRepository, // The object (value) we want to replace it with
                }, // The thing we're testing
                {
                    provide: TransformService, // The repository token (instead of the class)
                    useValue: transformService, // The object (value) we want to replace it with
                },
            ],
        }).compile();

        service = module.get<CommentService>(CommentService);
    });
    it('should be defined', () => {
        expect(service).toBeDefined();
    });

    describe('findAll', () => {
        it('should return correct result', async () => {
            const fakeFindCommentsArr = jest.spyOn(commentRepository, 'find')
                .mockImplementation(() => Promise.resolve(fakeCommentArr));
            const fakeTransformCommentArr = jest.spyOn(transformService, 'transformCommentArray')
                .mockImplementation(() => Promise.resolve(['result']));

            const result = await service.findAll(fakePost);

            expect(result).toEqual(['result']);

            fakeFindCommentsArr.mockRestore();
            fakeTransformCommentArr.mockRestore();
        });

        it('should call commentRepository.find() method with a particular argument', async () => {
            const find = jest.spyOn(commentRepository, 'find');
            service.findAll({ fakePost });
            expect(find).toHaveBeenCalled();
            find.mockRestore();
        });

        it('should call trasnformService.transformCommentArray() with correct parameter when .find returns a result', async () => {
            const transformCommentArray = jest.spyOn(transformService, 'transformCommentArray');
            await service.findAll(fakeCommentArr);
            expect(transformCommentArray).toHaveBeenCalled();
            transformCommentArray.mockRestore();
        });
    });

    describe('findCommentById', () => {
        it('should call the findOne method', async () => {
            const findOne = jest.spyOn(commentRepository, 'findOne')
                .mockImplementation(() => Promise.resolve(fakeComment));

            await service.findCommentById('3');

            expect(findOne).toHaveBeenCalled();

            findOne.mockRestore();
        });
        it('should return a comment which is found with the findOne method', async () => {
            const fakeCommentFound = jest.spyOn(commentRepository, 'findOne')
                .mockImplementation(() => Promise.resolve(fakeComment));

            const result = await service.findCommentById('7');

            expect(result).toEqual(fakeComment);

            fakeCommentFound.mockRestore();
        });
        it('should throw an error if comment was not found by id', async () => {
            const fakeCommentFound = jest.spyOn(commentRepository, 'findOne')
                .mockImplementation(() => Promise.resolve(null));

            try {
                await service.findCommentById('7');
            } catch (err) {
                expect(err).toBeInstanceOf(CommentNotFound);
            }

            fakeCommentFound.mockRestore();
        });

    });

    describe('transformCommentForDisplay', () => {
        it('should return the transformed comment for display', async () => {

            // Check if the method returns the value passed by the mocked findOne method
            const fakeTransformComment = jest.spyOn(transformService, 'transformComment')
                .mockImplementation(() => Promise.resolve('result'));

            const result = await service.transformCommentForDisplay(fakeComment);

            expect(result).toEqual('result');

            fakeTransformComment.mockRestore();
        });

        it('should call trasnformService.transformComment()) with correct parameter', async () => {
            const transformComment = jest.spyOn(transformService, 'transformComment');
            await service.transformCommentForDisplay(fakeComment);
            expect(transformComment).toHaveBeenCalled();
            transformComment.mockRestore();
        });

    });

    describe('add', () => {
        it('should return the transformed created comment', async () => {
            const fakeUser = new User();
            const fakeCreateComment = new Comment();
            fakeCreateComment.content = fakeComment.content;
            fakeCreateComment.user = Promise.resolve(fakeUser);
            fakeCreateComment.post = Promise.resolve(fakePost);

            const fakeCreatedComment = jest.spyOn(commentRepository, 'save')
                .mockImplementation(() => Promise.resolve(fakeCreateComment));
            const fakeTransformComment = jest.spyOn(transformService, 'transformComment')
                .mockImplementation(() => Promise.resolve('comment'));

            const result = await service.add(fakeComment, fakeUser, fakePost);

            expect(result).toEqual('comment');

            fakeCreatedComment.mockRestore();
            fakeTransformComment.mockRestore();
        });
        it('should call commentRepository.save() method with a particular argument', async () => {
            const fakeUser = new User();

            const save = jest.spyOn(commentRepository, 'save');
            service.add(fakeComment, fakeUser, fakePost);

            expect(save).toHaveBeenCalled();

            save.mockRestore();
        });

        it('should call trasnformService.transformComment() with correct parameter when .find returns a result', async () => {
            const fakeUser = new User();

            const transformComment = jest.spyOn(transformService, 'transformComment');
            await service.add(fakeComment, fakeUser, fakePost);

            expect(transformComment).toHaveBeenCalled();

            transformComment.mockRestore();
        });
    });

    describe('checkAuthor', () => {
        it('should throw an error if condition is met', async () => {
            const fakeRequestUser = new User();
            const fakeCommentAuthor = new User();

            fakeRequestUser.id = 1;
            fakeRequestUser.role = UserRole.MEMBER;
            fakeCommentAuthor.id = 2;

            try {
                await service.checkAuthor(fakeCommentAuthor, fakeRequestUser);
            } catch (err) {
                expect(err).toBeInstanceOf(BlockedAccessException);
            }
        });
        it('should not throw when condition is not met', async () => {
            const fakeRequestUser = new User();
            const fakeCommentAuthor = new User();

            fakeRequestUser.id = 1;
            fakeRequestUser.role = UserRole.MEMBER;
            fakeCommentAuthor.id = 1;

            const result = await service.checkAuthor(fakeCommentAuthor, fakeRequestUser);
            expect(result).toBe(undefined);
        });
    });

    describe('updateCommentById', () => {
        it('should return the updated comment', async () => {
            const fakeId = '7';
            const fakeCommentToBeUpdated = { ...fakeComment, fakeId: Number(fakeId) };

            const fakeUpdatedComment = jest.spyOn(commentRepository, 'update')
                .mockImplementation(() => Promise.resolve(fakeCommentToBeUpdated));

            const result = await service.updateCommentById(fakeCommentToBeUpdated, fakeId);

            expect(result).toEqual(fakeCommentToBeUpdated);

            fakeUpdatedComment.mockRestore();
        });
        it('should call commentRepository.update() method with a particular argument', async () => {
            const fakeId = '7';
            const fakeCommentToBeUpdated = { ...fakeComment, fakeId: Number(fakeId) };

            const update = jest.spyOn(commentRepository, 'update');
            service.updateCommentById(fakeCommentToBeUpdated, fakeId);

            expect(update).toHaveBeenCalled();

            update.mockRestore();
        });
    });

    describe('deleteCommentById', () => {
        it('should return the updated comment change delete field', async () => {
            const fakeCommentToBeDeleted = { isDeleted: true };

            const fakeUpdatedCommentDeletedField = jest.spyOn(commentRepository, 'update')
                .mockImplementation(() => Promise.resolve(fakeCommentToBeDeleted));

            const result = await service.deleteCommentById(fakeCommentToBeDeleted);

            expect(result).toEqual(fakeCommentToBeDeleted);

            fakeUpdatedCommentDeletedField.mockRestore();
        });
        it('should call commentRepository.update() method with a particular argument', async () => {
            const fakeCommentToBeDeleted = { isDeleted: true };

            const update = jest.spyOn(commentRepository, 'update');
            service.deleteCommentById(fakeCommentToBeDeleted);

            expect(update).toHaveBeenCalled();

            update.mockRestore();
        });
    });
});
