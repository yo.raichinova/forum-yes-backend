
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, UpdateResult } from 'typeorm';
import { Comment } from '../entities/comment.entity';
import { CommentCreateDTO } from '../models/comment-create-dto';
import { User } from '../entities/user.entity';
import { Posst } from '../entities/post.entity';
import { CommentNotFound, BlockedAccessException } from '../common/filters/post-system-exception';
import { CommentDisplayDTO } from '../models/comment-display-dto';
import { TransformService } from '../core/services/transform.service';

@Injectable()
export class CommentService {
  constructor(
    @InjectRepository(Comment)
    private readonly commentRepository: Repository<Comment>,
    private readonly transformService: TransformService,
  ) { }

  async findAll(post): Promise<CommentDisplayDTO[]> {
    const commentArr = await this.commentRepository.find({ post });
    const newArr = await this.transformService.transformCommentArray(commentArr);
    return newArr;
  }

  async findCommentById(id: string): Promise<Comment> | undefined {
    const commentFound = await this.commentRepository.findOne({ id: +id, isDeleted: false });
    if (!commentFound) {
      throw new CommentNotFound('Comment with such id does not exist');
    }
    return commentFound;
  }

  async transformCommentForDisplay(comment: Comment): Promise<CommentDisplayDTO> {
    return await this.transformService.transformComment(comment);
  }

  async add(comment: CommentCreateDTO, user: User, post: Posst): Promise<CommentDisplayDTO> {
    const createComment = new Comment();
    createComment.content = comment.content;
    createComment.user = Promise.resolve(user);
    createComment.post = Promise.resolve(post);
    const createdComment = await this.commentRepository.save(createComment);
    return await this.transformService.transformComment(createdComment);
  }

  async checkAuthor(commentAuthor: User, requestUser: User): Promise<void> {
    if (requestUser.role === 'member' && requestUser.id !== commentAuthor.id) {
      throw new BlockedAccessException('A user can only update or delete their own comments');
    }
  }

  async updateCommentById(comment: CommentCreateDTO, id: string): Promise<{ message: string }> {
    const commentToBeUpdated = { ...comment, id: Number(id) };
    // return await this.commentRepository.update(commentToBeUpdated.id, commentToBeUpdated);
    await this.commentRepository.update(commentToBeUpdated.id, commentToBeUpdated);
    return { message: 'Comment successfully updated!' };
  }

  async deleteCommentById(commentToBeDeleted): Promise<{ message: string }> {
    const commentChangedDeleteField = { isDeleted: true };
    // return await this.commentRepository.update(commentToBeDeleted.id, commentChangedDeleteField);
    await this.commentRepository.update(commentToBeDeleted.id, commentChangedDeleteField);
    return { message: 'Comment successfully deleted!' };
  }
  async flagCommentById(flaggedComment: Comment): Promise<UpdateResult> {
    const updatedField = { isFlagged: true };
    return await this.commentRepository.update(flaggedComment.id, updatedField);
  }

  async lockCommentById(lockComment: Comment): Promise<UpdateResult> {
    const updatedField = { isLocked: !lockComment.isLocked };
    return await this.commentRepository.update(lockComment.id, updatedField);
  }
}
