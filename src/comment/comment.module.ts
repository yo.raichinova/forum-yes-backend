import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CommentService } from './comment.service';
import { CommentController } from './comment.controller';
import { Comment } from '../entities/comment.entity';
import { CoreModule } from '../core/core.module';
import { AuthModule } from '../auth/auth.module';
import { PostModule } from '../post/post.module';
import { User } from '../entities/user.entity';
import { Posst } from '../entities/post.entity';

@Module({
    imports: [TypeOrmModule.forFeature([Comment, Posst, User]), CoreModule, AuthModule, PostModule],
    providers: [CommentService],
    controllers: [CommentController],
})
export class CommentModule { }
