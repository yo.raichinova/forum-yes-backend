<p><b>FORUM YES PROJECT</b></p>
by Elena Stoyanova, Yoana Raychinova, Alex Tagarev

<b>Main features:</b>
- User Registration, Login, Logout
- Posts
- Comments
- Unit Testing

<b>Additional features:</b>
- Admin - CRUD on posts/comments, Ban, Delete, Lock posts from changing
- Likes/Dislikes
- Friends
- Authorization
- Flag of posts






Trello Board: https://trello.com/b/TXE4nhsR/forumyes-project