import {MigrationInterface, QueryRunner} from "typeorm";

export class initial1561203459263 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("CREATE TABLE `status` (`id` int NOT NULL AUTO_INCREMENT, `isBanned` tinyint NOT NULL DEFAULT 0, `description` varchar(500) NOT NULL DEFAULT 'this user has not misbehaved', PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `user` (`id` int NOT NULL AUTO_INCREMENT, `name` varchar(50) NOT NULL, `password` varchar(200) NOT NULL, `email` varchar(50) NOT NULL, `role` enum ('admin', 'member') NOT NULL DEFAULT 'member', `isDeleted` tinyint NOT NULL DEFAULT 0, `statusId` int NULL, UNIQUE INDEX `REL_dc18daa696860586ba4667a9d3` (`statusId`), PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `posst` (`id` int NOT NULL AUTO_INCREMENT, `title` varchar(200) NOT NULL, `content` varchar(2500) NOT NULL, `createdAt` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `updatedAt` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `isDeleted` tinyint NOT NULL DEFAULT 0, `isFlagged` tinyint NOT NULL DEFAULT 0, `isLocked` tinyint NOT NULL DEFAULT 0, `userId` int NULL, FULLTEXT INDEX `IDX_e58f76bd4de8ddcdb14687ff10` (`title`), FULLTEXT INDEX `IDX_282132554cc62c7bf9c23fbe17` (`content`), PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `comment` (`id` int NOT NULL AUTO_INCREMENT, `content` text NOT NULL, `createdAt` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `updatedAt` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `isDeleted` tinyint NOT NULL DEFAULT 0, `isFlagged` tinyint NOT NULL DEFAULT 0, `isLocked` tinyint NOT NULL DEFAULT 0, `userId` int NULL, `postId` int NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `user_stalking_user` (`userId_1` int NOT NULL, `userId_2` int NOT NULL, INDEX `IDX_ef7a154cc4d3abb24fe94b1685` (`userId_1`), INDEX `IDX_deb2115f1ba2d4beff884f60e9` (`userId_2`), PRIMARY KEY (`userId_1`, `userId_2`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `posst_liked_by_user` (`posstId` int NOT NULL, `userId` int NOT NULL, INDEX `IDX_ac6674a0dbb41af536ca642d2a` (`posstId`), INDEX `IDX_2f3d2ad0958113bc892b9e69f7` (`userId`), PRIMARY KEY (`posstId`, `userId`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `posst_disliked_by_user` (`posstId` int NOT NULL, `userId` int NOT NULL, INDEX `IDX_db15e491108ab40305af345149` (`posstId`), INDEX `IDX_19cb11088d3d5021cbc3f3afbd` (`userId`), PRIMARY KEY (`posstId`, `userId`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `comment_liked_by_user` (`commentId` int NOT NULL, `userId` int NOT NULL, INDEX `IDX_8c43719f5483d3f2dd1e4d1ef0` (`commentId`), INDEX `IDX_dae8402ae22f4bfa0dcabda3c6` (`userId`), PRIMARY KEY (`commentId`, `userId`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `comment_disliked_by_user` (`commentId` int NOT NULL, `userId` int NOT NULL, INDEX `IDX_f5ae21db6cbd444790023e21f8` (`commentId`), INDEX `IDX_71c22350468d699454d3b3227e` (`userId`), PRIMARY KEY (`commentId`, `userId`)) ENGINE=InnoDB");
        await queryRunner.query("ALTER TABLE `user` ADD CONSTRAINT `FK_dc18daa696860586ba4667a9d31` FOREIGN KEY (`statusId`) REFERENCES `status`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `posst` ADD CONSTRAINT `FK_7b151bb731e80c5196aab637d77` FOREIGN KEY (`userId`) REFERENCES `user`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `comment` ADD CONSTRAINT `FK_c0354a9a009d3bb45a08655ce3b` FOREIGN KEY (`userId`) REFERENCES `user`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `comment` ADD CONSTRAINT `FK_94a85bb16d24033a2afdd5df060` FOREIGN KEY (`postId`) REFERENCES `posst`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `user_stalking_user` ADD CONSTRAINT `FK_ef7a154cc4d3abb24fe94b1685e` FOREIGN KEY (`userId_1`) REFERENCES `user`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `user_stalking_user` ADD CONSTRAINT `FK_deb2115f1ba2d4beff884f60e9d` FOREIGN KEY (`userId_2`) REFERENCES `user`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `posst_liked_by_user` ADD CONSTRAINT `FK_ac6674a0dbb41af536ca642d2a6` FOREIGN KEY (`posstId`) REFERENCES `posst`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `posst_liked_by_user` ADD CONSTRAINT `FK_2f3d2ad0958113bc892b9e69f7c` FOREIGN KEY (`userId`) REFERENCES `user`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `posst_disliked_by_user` ADD CONSTRAINT `FK_db15e491108ab40305af3451494` FOREIGN KEY (`posstId`) REFERENCES `posst`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `posst_disliked_by_user` ADD CONSTRAINT `FK_19cb11088d3d5021cbc3f3afbd4` FOREIGN KEY (`userId`) REFERENCES `user`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `comment_liked_by_user` ADD CONSTRAINT `FK_8c43719f5483d3f2dd1e4d1ef00` FOREIGN KEY (`commentId`) REFERENCES `comment`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `comment_liked_by_user` ADD CONSTRAINT `FK_dae8402ae22f4bfa0dcabda3c6c` FOREIGN KEY (`userId`) REFERENCES `user`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `comment_disliked_by_user` ADD CONSTRAINT `FK_f5ae21db6cbd444790023e21f8a` FOREIGN KEY (`commentId`) REFERENCES `comment`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `comment_disliked_by_user` ADD CONSTRAINT `FK_71c22350468d699454d3b3227eb` FOREIGN KEY (`userId`) REFERENCES `user`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION");
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `comment_disliked_by_user` DROP FOREIGN KEY `FK_71c22350468d699454d3b3227eb`");
        await queryRunner.query("ALTER TABLE `comment_disliked_by_user` DROP FOREIGN KEY `FK_f5ae21db6cbd444790023e21f8a`");
        await queryRunner.query("ALTER TABLE `comment_liked_by_user` DROP FOREIGN KEY `FK_dae8402ae22f4bfa0dcabda3c6c`");
        await queryRunner.query("ALTER TABLE `comment_liked_by_user` DROP FOREIGN KEY `FK_8c43719f5483d3f2dd1e4d1ef00`");
        await queryRunner.query("ALTER TABLE `posst_disliked_by_user` DROP FOREIGN KEY `FK_19cb11088d3d5021cbc3f3afbd4`");
        await queryRunner.query("ALTER TABLE `posst_disliked_by_user` DROP FOREIGN KEY `FK_db15e491108ab40305af3451494`");
        await queryRunner.query("ALTER TABLE `posst_liked_by_user` DROP FOREIGN KEY `FK_2f3d2ad0958113bc892b9e69f7c`");
        await queryRunner.query("ALTER TABLE `posst_liked_by_user` DROP FOREIGN KEY `FK_ac6674a0dbb41af536ca642d2a6`");
        await queryRunner.query("ALTER TABLE `user_stalking_user` DROP FOREIGN KEY `FK_deb2115f1ba2d4beff884f60e9d`");
        await queryRunner.query("ALTER TABLE `user_stalking_user` DROP FOREIGN KEY `FK_ef7a154cc4d3abb24fe94b1685e`");
        await queryRunner.query("ALTER TABLE `comment` DROP FOREIGN KEY `FK_94a85bb16d24033a2afdd5df060`");
        await queryRunner.query("ALTER TABLE `comment` DROP FOREIGN KEY `FK_c0354a9a009d3bb45a08655ce3b`");
        await queryRunner.query("ALTER TABLE `posst` DROP FOREIGN KEY `FK_7b151bb731e80c5196aab637d77`");
        await queryRunner.query("ALTER TABLE `user` DROP FOREIGN KEY `FK_dc18daa696860586ba4667a9d31`");
        await queryRunner.query("DROP INDEX `IDX_71c22350468d699454d3b3227e` ON `comment_disliked_by_user`");
        await queryRunner.query("DROP INDEX `IDX_f5ae21db6cbd444790023e21f8` ON `comment_disliked_by_user`");
        await queryRunner.query("DROP TABLE `comment_disliked_by_user`");
        await queryRunner.query("DROP INDEX `IDX_dae8402ae22f4bfa0dcabda3c6` ON `comment_liked_by_user`");
        await queryRunner.query("DROP INDEX `IDX_8c43719f5483d3f2dd1e4d1ef0` ON `comment_liked_by_user`");
        await queryRunner.query("DROP TABLE `comment_liked_by_user`");
        await queryRunner.query("DROP INDEX `IDX_19cb11088d3d5021cbc3f3afbd` ON `posst_disliked_by_user`");
        await queryRunner.query("DROP INDEX `IDX_db15e491108ab40305af345149` ON `posst_disliked_by_user`");
        await queryRunner.query("DROP TABLE `posst_disliked_by_user`");
        await queryRunner.query("DROP INDEX `IDX_2f3d2ad0958113bc892b9e69f7` ON `posst_liked_by_user`");
        await queryRunner.query("DROP INDEX `IDX_ac6674a0dbb41af536ca642d2a` ON `posst_liked_by_user`");
        await queryRunner.query("DROP TABLE `posst_liked_by_user`");
        await queryRunner.query("DROP INDEX `IDX_deb2115f1ba2d4beff884f60e9` ON `user_stalking_user`");
        await queryRunner.query("DROP INDEX `IDX_ef7a154cc4d3abb24fe94b1685` ON `user_stalking_user`");
        await queryRunner.query("DROP TABLE `user_stalking_user`");
        await queryRunner.query("DROP TABLE `comment`");
        await queryRunner.query("DROP INDEX `IDX_282132554cc62c7bf9c23fbe17` ON `posst`");
        await queryRunner.query("DROP INDEX `IDX_e58f76bd4de8ddcdb14687ff10` ON `posst`");
        await queryRunner.query("DROP TABLE `posst`");
        await queryRunner.query("DROP INDEX `REL_dc18daa696860586ba4667a9d3` ON `user`");
        await queryRunner.query("DROP TABLE `user`");
        await queryRunner.query("DROP TABLE `status`");
    }

}
