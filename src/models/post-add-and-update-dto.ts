import { IsString, Length } from 'class-validator';

export class PostAddAndUpdateDTO {
    @IsString()
    @Length(1, 70, {
        message: 'Tittle should not be longer than 70 characters.',
    })
    title: string;

    @IsString()
    // @Length(1, 300, {
    //     message: 'Post should not be longer than 300 characters.',
    // })
    content: string;
}
