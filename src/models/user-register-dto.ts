import { IsEmail, IsString, Matches, Length } from 'class-validator';

export class UserRegisterDTO {
    @IsString()
    @Length(1, 30, {
        message: 'Name should be between 1 and 30 characters',
    })
    name: string;

    @IsEmail()
    @Length(1, 50, {
        message: 'Email should be at least 5 characters long.',
    })
    email: string;

    @IsString()
    @Length(3, 30, {
        message: 'Password should be at least 3 characters long.',
    })
    // @Matches(/(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{6,}/)
    password: string;
}
