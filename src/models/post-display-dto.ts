import { Expose } from 'class-transformer';
import { IsString, IsDate, IsNumber, IsBoolean } from 'class-validator';
import { CommentDisplayDTO } from './comment-display-dto';

export class PostDisplayDTO {
    @Expose()
    @IsNumber()
    id: number;

    @Expose()
    @IsString()
    title: string;

    @Expose()
    @IsString()
    content: string;

    @Expose()
    @IsString()
    user: string;

    @Expose()
    @IsDate()
    createdAt: Date;

    @Expose()
    @IsDate()
    updatedAt?: Date;

    // @Expose()
    // comments: CommentDisplayDTO[]

    @Expose()
    @IsNumber()
    likedBy: number;

    @Expose()
    @IsNumber()
    dislikedBy: number;

    @Expose()
    @IsBoolean()
    isFlagged: boolean;

    @Expose()
    @IsBoolean()
    isLocked: boolean;

}
