import { IsString, MinLength } from 'class-validator';

export class StatusDescribeDTO {
    @IsString()
    @MinLength(5)
    content: string;
}
