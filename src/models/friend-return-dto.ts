import { Expose } from "class-transformer";
import { IsString, IsNumber } from "class-validator";

export class FriendReturnDTO {
    @Expose()
    @IsNumber()
    id: number;

    @Expose()
    @IsString()
    name: string;

    @Expose()
    @IsString()
    email: string;
}