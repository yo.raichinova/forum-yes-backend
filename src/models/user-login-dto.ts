import { IsEmail, IsString, Matches, MinLength, Length } from 'class-validator';

export class UserLoginDTO {
    @IsString()
    @Length(1, 30, {
        message: 'Name should be between 1 and 30 characters',
    })
    name: string;

    @IsString()
    @Length(3, 30, {
        message: 'Password should be at least 3 characters long.',
    })
    // @Matches(/(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{6,}/)
    password: string;
}
