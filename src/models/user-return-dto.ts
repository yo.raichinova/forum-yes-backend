import { IsString, IsNumber } from 'class-validator';
import { Expose } from 'class-transformer';
import { UserRole } from '../entities/user.entity';
import { PostDisplayDTO } from './post-display-dto';
import { CommentDisplayDTO } from './comment-display-dto';
import { FriendReturnDTO } from './friend-return-dto';
import { Status } from '../entities/status.entity';

export class UserReturnDTO {
    // @Expose()
    // @IsString()
    // name: string;
    @Expose()
    @IsNumber()
    id: number;

    @Expose()
    @IsString()
    name: string;

    @Expose()
    @IsString()
    email: string;

    @Expose()
    role: UserRole;

    @Expose()
    status: Status;

    @Expose()
    posts: PostDisplayDTO[];

    @Expose()
    comments: CommentDisplayDTO[];

    @Expose()
    friends: FriendReturnDTO[];

    @Expose()
    postLikes: number[];

    @Expose()
    postDislikes: number[];

    @Expose()
    commentLikes: number[];

    @Expose()
    commentDislikes: number[];
}
