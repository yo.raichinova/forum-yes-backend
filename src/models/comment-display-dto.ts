import { IsString, IsNumber, IsBoolean, IsDate } from 'class-validator';
import { Expose } from 'class-transformer';

export class CommentDisplayDTO {
    @Expose()
    @IsNumber()
    id: number;

    @Expose()
    @IsString()
    content: string;

    @Expose()
    @IsString()
    user: string;

    @Expose()
    @IsDate()
    createdAt: Date;

    @Expose()
    @IsDate()
    updatedAt: Date;

    @Expose()
    @IsNumber()
    likedBy: number;

    @Expose()
    @IsNumber()
    dislikedBy: number;

    @Expose()
    @IsBoolean()
    isFlagged: boolean;

    @Expose()
    @IsBoolean()
    isLocked: boolean;
}
