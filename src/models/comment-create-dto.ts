import { IsString, Length } from 'class-validator';
import { Expose } from 'class-transformer';

export class CommentCreateDTO {
    @Expose()
    @IsString()
    @Length(1, 300, {
        message: 'Comment should not be longer than 300 characters.',
    })
    content: string;
}
