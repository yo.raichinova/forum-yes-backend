import { IsString, Length } from 'class-validator';
import { Expose } from 'class-transformer';
import { User } from '../entities/user.entity';
import { Posst } from '../entities/post.entity';

export class CommentAddAndShowDTO {
    @Expose()
    @IsString()
    @Length(1, 300, {
        message: 'Comment should not be longer than 300 characters.',
    })
    content: string;

    @Expose()
    user: User;

    post: Posst;
}
