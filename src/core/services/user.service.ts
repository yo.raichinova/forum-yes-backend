
import { Injectable, BadRequestException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, Like } from 'typeorm';
import { UserLoginDTO } from '../../models/user-login-dto';
import { JwtPayload } from '../interfaces/jwt-payload';
import { UserRegisterDTO } from '../../models/user-register-dto';
import * as bcrypt from 'bcrypt';
import { User } from '../../entities/user.entity';
import { Status } from '../../entities/status.entity';
import { plainToClass } from 'class-transformer';
import { UserReturnDTO } from '../../models/user-return-dto';
import { UserNotFound, BlockedAccessException, BadRequest } from '../../common/filters/post-system-exception';
import { UserQueryDTO } from '../../models/user-query-dto';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(User)
    private readonly userRepository: Repository<User>,
  ) { }

  async findAll(query: UserQueryDTO): Promise<User[]> {
    if (query.text) {
      const searchTerm = query.text;

      const result = await this.userRepository.find({
        where: [
          { name: Like(`%${searchTerm}%`), isDeleted: false },
        ],
      });

      if (result.length === 0) {
        throw new UserNotFound('No users with this name were found');
      }
      return result;
    }
    return await this.userRepository.find({where: { isDeleted: false }});
  }

  async findUserById(id: string): Promise<User> | undefined {
    const userFound = await this.userRepository.findOne({ id: +id, isDeleted: false });
    if (!userFound) {
      throw new UserNotFound('User with this id does not exist');
    }
    return userFound;
  }

  async findUserByUsername(username: string): Promise<User> {
    const foundUser = await this.userRepository.findOne({ name: username });
    return foundUser;
  }

  async addUser(user: UserRegisterDTO, userStatus: Status): Promise<UserReturnDTO> {

    const passwordHash = await bcrypt.hash(user.password, 10);

    const userToBeRegistered = new User();
    userToBeRegistered.name = user.name;
    userToBeRegistered.password = passwordHash;
    userToBeRegistered.email = user.email;
    userToBeRegistered.status = Promise.resolve(userStatus);
    const createdUser = await this.userRepository.save(userToBeRegistered);

    return plainToClass(UserReturnDTO, createdUser, { excludeExtraneousValues: true });
  }

  async saveUser(user: User): Promise<User> {
    return this.userRepository.save(user);
  }

  async addFriend(user: UserReturnDTO, userToBeStalkedId: number): Promise<User> | undefined {
    const stalkingUser: User = await this.userRepository.findOne({ name: user.name, isDeleted: false });
    const userToBeStalked: User = await this.userRepository.findOne({ id: userToBeStalkedId, isDeleted: false });
    const stalkingList: User[] = await stalkingUser.stalking;
    const indexOfStalkedUser = await this.isUserStalkingUser(userToBeStalked, stalkingList);
    if (!indexOfStalkedUser) {
      stalkingUser.stalking = Promise.resolve([userToBeStalked, ...stalkingList]);
      return await this.userRepository.save(stalkingUser);
      // return `${await userToBeStalked.name} has been added to your friends list.`.toString();
    } else {
      throw new BadRequestException('This user is already on your friends list.');
    }
  }

  async removeFriend(userToBeStalkedId: number, requestUser): Promise<User> | undefined {
    const userToStopBeingStalked = await this.userRepository.findOne({ id: userToBeStalkedId, isDeleted: false });
    const stalkedByList = await userToStopBeingStalked.stalkedBy;
    const stalkerToStop = stalkedByList.find((user) => user.id === requestUser.id);

    stalkedByList.splice(stalkedByList.indexOf(stalkerToStop), 1);
    userToStopBeingStalked.stalkedBy = Promise.resolve([...stalkedByList]);
    return await this.userRepository.save(userToStopBeingStalked);
    // return `${await userToStopBeingStalked.name} has been removed from your friends list.`;
  }

async isUserStalkingUser(stalking: User, stalkingArray: User[]): Promise < number > {
  let indexOfStalkedUser: number;
  stalkingArray.forEach((userToBeChecked: User, index: number) => {
    if (userToBeChecked.name === stalking.name) {
      indexOfStalkedUser = index;
    }
  });
  return indexOfStalkedUser;
}

async validateUserPassword(user: UserLoginDTO): Promise < boolean > {

  const userEntity = await this.userRepository.findOne({ name: user.name });

  const validPassword = await bcrypt.compare(user.password, userEntity.password);
  if (validPassword === false) {
  throw new BadRequest('Invalid password!');
}
  return validPassword;
  }

async validate(payload: JwtPayload): Promise<User> | undefined {
  return await this.userRepository.findOne(payload);
}

async deleteUser(userForDeletion: User): Promise <{ message: string }> {
    if (userForDeletion.role === 'admin') {
      throw new BlockedAccessException('An admin cannot delete another admin');
    }
    const userChangedDeleteField: User = { ...userForDeletion, isDeleted: true };
    await this.userRepository.update(userChangedDeleteField.id, userChangedDeleteField);
    return { message: 'Delete successful!'};
  }

}
