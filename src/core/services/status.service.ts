
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { User } from '../../entities/user.entity';
import { Status } from '../../entities/status.entity';
import { StatusDescribeDTO } from '../../models/status-describe-dto';
import { BlockedAccessException } from '../../common/filters/post-system-exception';

@Injectable()
export class StatusService {
    constructor(
        @InjectRepository(Status)
        private readonly statusRepository: Repository<Status>,

    ) { }

    async saveStatus(status: Status): Promise<Status> {
        return this.statusRepository.save(status);
    }

    async updateStatus(description: StatusDescribeDTO, user: User): Promise<User> {
        if (user.role === 'admin') {
            throw new BlockedAccessException('You cannot block another admin user!');
        }

        const userStatus = await user.status;
        userStatus.description = description.content;
        userStatus.isBanned = (!userStatus.isBanned);

        await this.statusRepository.save(userStatus);
        return user;
    }
}
