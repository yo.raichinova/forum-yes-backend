import { Posst } from '../../entities/post.entity';
import { Comment } from '../../entities/comment.entity';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { Injectable } from '@nestjs/common';
import { User } from '../../entities/user.entity';
import { BlockedAccessException } from '../../common/filters/post-system-exception';

@Injectable()
export class LikesService {
    constructor(
        @InjectRepository(Posst)
        private readonly postRepository: Repository<Posst>,
        @InjectRepository(Comment)
        private readonly commentRepository: Repository<Comment>,
    ) { }

    async checkAuthor(contentFound, requestUser): Promise<void> {
        const contentAuthor = await contentFound.user;
        if (requestUser.id === contentAuthor.id) {
            throw new BlockedAccessException('User cannot vote for their own content');
        }
    }
    async checkPreviousVotes(field: Promise<User[]>, requestUser: User): Promise<User | undefined> {
        const contentVotes = await field;             // returns an array of Users who have liked/disliked this content before
        return contentVotes.find((user) => user.id === requestUser.id); // returns either a User, or undefined
    }
    async spliceOppVotes(oppField: Promise<User[]>, userHasOppVotedBefore: User): Promise<User[]> {
        const contentOppVotes = await oppField;
        contentOppVotes.splice(contentOppVotes.indexOf(userHasOppVotedBefore), 1);  // remove user from likedBy <User[]>
        return oppField = Promise.resolve([...contentOppVotes]);
    }

    async prepareForVoting(contentFound: Posst | Comment, requestUser: User, field: Promise<User[]>, oppField: Promise<User[]>): Promise<void> {

        await this.checkAuthor(contentFound, requestUser);

        const userHasVotedBefore = await this.checkPreviousVotes(field, requestUser);
        if (userHasVotedBefore) {
            throw new BlockedAccessException('You have already voted for this content before');
        }

        const userHasOppVotedBefore = await this.checkPreviousVotes(oppField, requestUser); // returns either a User or undefined
        if (userHasOppVotedBefore) {
            await this.spliceOppVotes(oppField, await userHasOppVotedBefore);
        }
    }

    async upvotePost(post: Posst, user: User): Promise<Posst> {
        post.likedBy = Promise.resolve([user, ...await post.likedBy]);
        user.postLikes = Promise.resolve([post, ...await user.postLikes]);
        return this.postRepository.save(post);
    }

    async downvotePost(post: Posst, user: User): Promise<Posst> {
        post.dislikedBy = Promise.resolve([user, ...await post.dislikedBy]);
        user.postDislikes = Promise.resolve([post, ...await user.postDislikes]);
        return this.postRepository.save(post);
    }

    async upvoteComment(comment: Comment, user: User): Promise<Comment> {
        comment.likedBy = Promise.resolve([user, ...await comment.likedBy]);
        user.commentLikes = Promise.resolve([comment, ...await user.commentLikes]);
        return this.commentRepository.save(comment);
    }
    async downvoteComment(comment: Comment, user: User): Promise<Comment> {
        comment.dislikedBy = Promise.resolve([user, ...await comment.dislikedBy]);
        user.commentDislikes = Promise.resolve([comment, ...await user.commentDislikes]);
        return this.commentRepository.save(comment);
    }
}
