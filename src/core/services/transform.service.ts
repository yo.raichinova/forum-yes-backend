import { Injectable } from '@nestjs/common';
import { Posst } from '../../entities/post.entity';
import { PostDisplayDTO } from '../../models/post-display-dto';
import { plainToClass } from 'class-transformer';
import { CommentDisplayDTO } from '../../models/comment-display-dto';
import { User } from '../../entities/user.entity';
import { Comment } from '../../entities/comment.entity';
import { UserReturnDTO } from '../../models/user-return-dto';
import { FriendReturnDTO } from '../../models/friend-return-dto';
import { Status } from '../../entities/status.entity';

@Injectable()
export class TransformService {

    async transformPostArray(postArr: Posst[]): Promise<PostDisplayDTO[]> {
        const newArr = postArr.map(async (post: Posst) => {
            const postDisplayed: PostDisplayDTO = await this.prepareEntityForDisplay(post);
            return plainToClass(PostDisplayDTO, postDisplayed, { excludeExtraneousValues: true });
        });
        return Promise.all(newArr);
    }

    async transformPost(post: Posst): Promise<PostDisplayDTO> {
        const postDisplayed: PostDisplayDTO = await this.prepareEntityForDisplay(post);
        const postTransformed = plainToClass(PostDisplayDTO, postDisplayed, { excludeExtraneousValues: true });
        const commentListAllComments = await post.comments;
        const commentList = commentListAllComments.filter((comment) => comment.isDeleted === false);
        const transformedCommentList = await this.transformCommentArray(commentList);
        const postWithComments = {...postTransformed, comments: transformedCommentList};
        return postWithComments;
    }
    async prepareEntityForDisplay(entity: Posst|Comment): Promise<any> {
        const entityId = entity.id;
        const entityAuthor = await entity.user;
        const entityUserName = entityAuthor.name;
        const likes = await entity.likedBy;
        const likesNumber = likes.length;
        const dislikes = await entity.dislikedBy;
        const dislikesNumber = dislikes.length;
        const isEntityFlagged = entity.isFlagged;
        const isEntityLocked = entity.isLocked;
        const entityDisplayed = { ...entity, id: entityId, user: entityUserName, likedBy: likesNumber, dislikedBy: dislikesNumber, isFlagged: isEntityFlagged, isLocked: isEntityLocked };
        return entityDisplayed;
    }

    async transformComment(comment: Comment): Promise<CommentDisplayDTO> {
        const commentDisplayed: CommentDisplayDTO = await this.prepareEntityForDisplay(comment);
        return plainToClass(CommentDisplayDTO, commentDisplayed, { excludeExtraneousValues: true });
    }

    async transformCommentArray(commentArr: Comment[]): Promise<CommentDisplayDTO[]> {
        const newArr = commentArr.map(async (comment: Comment) => {
            const commentDisplayed: PostDisplayDTO = await this.prepareEntityForDisplay(comment);
            return plainToClass(CommentDisplayDTO, commentDisplayed, { excludeExtraneousValues: true });
        });
        return Promise.all(newArr);
    }

    async transformUserArray(userArr): Promise<UserReturnDTO[]> {
        const transformedArr = userArr.map(async (user) => {
            const userPosts = await user.posts;
            const userComments = await user.comments;
            const userFriends = await user.stalking;
            const userStatus: Status = await user.status;
            const postLikes: number[] = (await user.postLikes).map(post => post.id);
            const postDislikes: number[] = (await user.postDislikes).map(post => post.id);
            const commentLikes: number[] = (await user.commentLikes).map(comment => comment.id);
            const commentDislikes: number[] = (await user.commentDislikes).map(comment => comment.id);
            const transfromedFriendsList: FriendReturnDTO[] = await this.transformFriendArray(userFriends);
            const transformedPostList: PostDisplayDTO[] = await this.transformPostArray(userPosts);
            const transformCommentList: CommentDisplayDTO[] = await this.transformCommentArray(userComments);
// tslint:disable-next-line: max-line-length
            const userDisplayed: UserReturnDTO = { id: user.id, name: user.name, email: user.email, role: user.role, status: userStatus, posts: transformedPostList, comments: transformCommentList, friends: transfromedFriendsList, postLikes, postDislikes, commentLikes, commentDislikes }
            return userDisplayed;

        });
        return Promise.all(transformedArr);
    }
    transformFriendArray(userArr): FriendReturnDTO[] {
        const transformedArr = userArr.map((user) => {
            return plainToClass(FriendReturnDTO, user, { excludeExtraneousValues: true });
        });
        return transformedArr;
    }

    async transformUser(userFound: User): Promise<UserReturnDTO> {
        const userPosts = await userFound.posts;
        const userComments = await userFound.comments;
        const userFriends = await userFound.stalking;
        const userStatus = await userFound.status;
        const postLikes: number[] = (await userFound.postLikes).map(post => post.id);
        const postDislikes: number[] = (await userFound.postDislikes).map(post => post.id);
        const commentLikes: number[] = (await userFound.commentLikes).map(comment => comment.id);
        const commentDislikes: number[] = (await userFound.commentDislikes).map(comment => comment.id);
        const transfromedFriendsList: FriendReturnDTO[] = await this.transformFriendArray(userFriends);
        const transformedPostList: PostDisplayDTO[] = await this.transformPostArray(userPosts);
        const transformCommentList: CommentDisplayDTO[] = await this.transformCommentArray(userComments);
        const userDisplayed: UserReturnDTO = { id: userFound.id, name: userFound.name, email: userFound.email, role: userFound.role, status: userStatus, posts: transformedPostList, comments: transformCommentList, friends: transfromedFriendsList, postLikes, postDislikes, commentLikes, commentDislikes};
        return userDisplayed;
    }

}
