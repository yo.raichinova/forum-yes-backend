import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserService } from './services/user.service';
import { User } from '../entities/user.entity';
import { StatusService } from './services/status.service';
import { Status } from '../entities/status.entity';
import { LikesService } from './services/likes.service';
import { Posst } from '../entities/post.entity';
import { Comment } from '../entities/comment.entity';
import { TransformService } from './services/transform.service';

@Module({
    imports: [TypeOrmModule.forFeature([User, Status, Posst, Comment])],
    providers: [UserService, StatusService, LikesService, TransformService],
    exports: [UserService, StatusService, LikesService, TransformService],
})
export class CoreModule {}
